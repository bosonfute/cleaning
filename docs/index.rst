************
cleaningtool
************
.. contents:: Table of Contents

Short description
=================

The goal of the package is to clean files in a given `parent_directory`.

1. For each file, a `CSVSniffer` looks at a random sample of the rows (in memory) and
infers the columns types and the potential operation to fix the values
(datetime to isoformat, etc.).

2. A `FileParser` then parses the entire file row by row to avoid memory consumption and
tries to fix values according to what the sniffer found about the column types.

3. The cleaned rows are then stored one after the other in a clean file, stored in a
clean_data directory mirroring  the structure of the `parent_directory`. Corrupted rows
(those that do not match the correct number of columns, or fields) are stored separately
the same directory.

4. Finally, in a metadata directory mirroring the structure of the `parent_directory`,
the files metadata are stored as a json. A csv (for now) metadata file for each data file
stores the following information for each of its columns:

* the number of distinct values
* the inferred type
* the fix operation the FileParser tried
* the number and percentage of errors encountered (fix did not work) (important to check in order to identify cleaning problems)
* the number and percentage of empty values
* the number and percentage of emptied values (by default, no replacement by empty value is done) in the case of errors with the column type
* statistics about each column (min, max, most frequent values)

5. Everything is logged and stored in a logs directory, mirroring the structure
of the `parent_directory`



Documentation
=============
.. toctree::
   :maxdepth: 3

   documentation
   todos

API Contents
============

.. toctree::
   :maxdepth: 3

   CSVcleaner
   scripts

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
