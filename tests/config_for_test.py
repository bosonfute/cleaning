"""
Config file for CSVcleaner
"""

######################
# Data files for tests
######################
testfile_encoding = 'sample_encoding.csv'
testfile_encoding2 = 'sample_encoding2.csv'
testfile_special_characters = 'sample_special_characters.csv'
testfile_sample_data_types = 'sample_data_types.csv'
testfile_sample_data_types_pathsinfo = 'sample_data_types_paths_info.pkl'
testfile_sample_data_types_filemetadata = 'sample_data_types_file_metadata.pkl'

############################
# FileMetadata configuration
############################
# nb of rows to consider when checking file
check_n_rows = 1000
# random rows: True if the check_n_rows have to be picked randomly from the csv file
check_random_rows = True
# percentage of non matching formats to accept a given format
check_acceptable_pct = 0.25
# delimiters to try while checking file
check_delimiters = ["|", ";", "\t", ","]
# values to be replaced by empty values and count as empty (i.e. nan)
ignore_values = []
# delimiter replacement: True if the parsing must try to replace the delimiter; False otherwise
delimiter_replacement = True
# target delimiter to use if delimiter_replacement=True
target_delimiter = '|'
# unimportant keys to ignore in file_metadata.__dict__ in order to compare two file_metadata.__dict__
file_metadata_ignore_keys = ['logger', 'paths_info', 'time']

# date, hour, datetime formats to try while checking file
date_formats = {
                'time_iso': ("%H:%M:%S", r'^[0-9]{2}:[0-9]{2}:[0-9]{2}$'),
                'date_iso': ("%Y-%m-%d", r'^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$'),
                'date_french': ("%d/%m/%Y", r'^[0-3]{0,1}[0-9]{1}/[0-1]{0,1}[0-9]{1}/[0-9]{4}$'),
                'date_us': ("%m/%d/%Y", r'^[0-1]{0,1}[0-9]{1}/[0-3]{0,1}[0-9]{1}/[0-9]{4}$'),
                'datetime_iso': ("%Y-%m-%d %H:%M:%S", \
                    r'^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2} [0-9]{2}:[0-9]{2}:[0-9]{2}$'),
                'datetime_hourmin': ("%Y-%m-%d %H:%M", \
                    r'^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2} [0-9]{2}:[0-9]{2}$'),
                'datetime_french_hourmin': ("%d/%m/%Y %H:%M", \
                    r'^[0-3]{0,1}[0-9]{1}/[0-1]{0,1}[0-9]{1}/[0-9]{4} [0-9]{2}:[0-9]{2}$'),
                'datetime_french': ("%d/%m/%Y %H:%M:%S", \
                    r'^[0-3]{0,1}[0-9]{1}/[0-1]{0,1}[0-9]{1}/[0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2}$')
               }

#####################################
# FileParser.parse_file configuration
#####################################
erase_unmatched_values = False

###########################################
# FileParser.basic_statistics configuration
###########################################
# up to how many most frequent values must be stores in the file metadata statistics
nb_most_frequent = 10
# round decimal: how many decimal to keep in statistics (pct, etc.)
round_decimal = 2
# statistics file delimiter
stats_file_delimiter = ';'
