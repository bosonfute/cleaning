#!/bin/bash
# Author: Heloise Nonne
# Last revised 2016-04-09
# Change all file names in a directory to lowercase, replace spaces with underscores and accent by ascii characters.

NB_ARGS=1
E_OPTERROR=85

# set internal field separator to strict-mode (filenames with space must be considered as a single item in the array)
IFS=$'\n\t'

parent_directory=$1


usage() {
    echo "usage: $0 directory"
    echo "Change all file names in a directory to lowercase, replace spaces with underscores and accent by ascii characters."
}


main() {
    # first change directory names

    # find deepest level in parent_directory
    maxdepth=$(find $parent_directory -type f | awk -F '/' '{ if (NF>depth) {depth=NF;}} END {print depth}')
    parentdepth=$(echo $parent_directory | awk -F '/' '{print NF}')
    maxdepth=$(($maxdepth - $parentdepth))

    # rename directory level after level (but do not change parent_directory case or name)
    for depth in $(seq 1 $maxdepth); do
        directories=($(find $parent_directory -maxdepth $depth -type d))

        for d in ${directories[*]}; do
            # get directory name after parent_directory (parent_directory name must remain unchanged)
            name=${d#$parent_directory}
            # tolowercase + replace spaces by _ and accent by ascii
            newname=$(echo $name | sed -e 's/\(.*\)/\L\1/' | sed -e 's/\s/_/g' | iconv -f utf8 -t ascii//TRANSLIT)
            if [ "$name" != "$newname" ]; then
                mv -f "$parent_directory$name" "$parent_directory$newname"
            fi
        done
    done

    # change file names
    # get all files
    files=($(find $parent_directory -type f))
    # rename files
    for f in ${files[*]}; do
        dirname=$(dirname $f)
        name=$(basename $f)
        newname=$(echo $name | sed -e 's/\(.*\)/\L\1/' | sed -e 's/\s/_/g' | iconv -f utf8 -t ascii//TRANSLIT)
        if [ "$name" != "$newname" ]; then
            mv "$dirname/$name" "$dirname/$newname"
        fi
    done
}


if [ $# -ne "$NB_ARGS" ]
then # script invoked with wrong nb of arguments
    usage
    exit $E_OPTERROR
fi

main
