Warnings and todos
==================

.. todo:: Development

    - Find better format for metadata (file metadata json and statistics as csv; formats should be improved)
    - fix todos inside code
    - raise exceptions ValueError when options are not what they are supposed to be


.. warning:: Check with use of the tool

    - check that the floats are correctly inferes (us, french, float)

.. todo:: Reported bugs

    - gestion cas 100%, 80%, etc. Conversion en int ??? cf field "% Avancement PHYSIQUE au 31 décembre" dans banque_travaux_suivi_programme...


.. todo:: Features priority should

    - make cleaning optional in `CSVcleaner.parsers.FileParser.parse_file` (ex: make just some stats)
    - set a dict of column data types before cleaning (manual input)
    - for efficiency, try to concatenate the 2 sed steps in scripts/fix_special_characters.sh
    - infer type anyway when there are many empty values
    - function to check if there is a problem in file encoding before parsing
    - fix EOL in comments (see png file from slack in file liste_armements)
    - improve regex of date_formats (more specific and more robust), for instance # %d/%m -> [0-3]{0,1}[0-9]{1}/[0-1]{0,1}[0-9]{1} and vice versa -> betting handling ambiguity between date_french and date_us in `CSVcleaner.parsers.data_types.try_transform_date_to_iso`
    - warning if header not found and file supposed to have header (in `CSVcleaner.CSVSniffer.FileMetadata.check_header`)


.. todo:: Features priority nice to have

    - nettoyage des noms de colonnes -> lowercase, no accent, no space
    - identify nb of bytes necessary to store the data (int8, int16, int64) + str max length
    - minuscules et suppression des accents dans les valeurs
    - NA possibles: "-", "Non renseigné", "na", "NC", etc. à tester dans `CSVcleaner.config.config.ignore_values`
    - classer VRAI/FAUX, OUI/NON en type boolean et gérer les small letters
    - datetime au format YYYY-MM-DD 00:00:00 -> detect and remove the time
    - remplacer les exposants en vrais chiffres (ex: col Ecarts dans banque_travaux_suivi_programme)


.. todo:: Features priority could have

    - stats supplémentaires : pour les dates : nb enregistrements par mois, par jour de la semaine, par année
    - int et float : détection d'outliers ?


.. todo:: Cleaning spécifique SNCF Reseau

    - ex: voie saisie dans banque_travaux_suivi_programme
        - 1 + 2, 1 et 2, 1 ET 2
    - ex: numéro de la voie. Cf champs "voie_i" dans banque_travaux_suivi_programme
    - ex: PKD PKF avec les + et les -
    - ex: col UIC dans banque_travaux_suivi_programme: non normé
    - text mining pour identifier la localisation à partir du texte
        - du km x au y, du km x au km y, du PK x au PK y
    - vérification que le code ligne appartient bien au référentiel


.. todo: Nouvel outil

    - faire des rapprochements entre tables/fichiers sur les métadonnées (même type, même nombre de valeurs possibles, même X valeurs plus importantes)
