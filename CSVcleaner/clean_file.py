#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Heloise Nonne
# Last revised 2017-03-10
"""
This modules cleans a given file by:
- changing file encoding to utf8
- removing special characters (non standard encoding, endoflines, etc.)
- sniffing the file to infer how its made and what it contains (fields, data types, date format)
- parsing the file and fixing values where it can and discarting corrupted rows
- it also compute basic statistics about the content of each column
- stores cleaned data, logs, metadata in corresponding directories
"""

import os
import sys

from datetime import datetime

import logging

sys.path.append('./')

import CSVcleaner.config.config as cfg

import CSVcleaner.parsers.CSVSniffer as sniffer
import CSVcleaner.parsers.FileParser as fp

import CSVcleaner.utils.logger_setup as logger_setup
import CSVcleaner.utils.misc as misc
import CSVcleaner.utils.bash_cleaning as bash_cleaning

def parse_and_clean_csv(file,
                        parent_directory):
    """ Parse and clean a csv file:
    - creates mirror trees directories to store metadata and clean data;
    - identifies characteristics of file (nb of lines, fields, types, etc.);
    - parses file to clean it and story in the mirror tree in clean directory
    - computes basic statistics about the columns contents and store it in the mirror tree
    in metadata directory

    :Parameters:
        - `file` (str)
            name of the file to clean
        - `parent_directory` (str)
            location of the file's parent directory

    :Return:
        - None
    """
    time = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")

    # create mirror tree directory from parent dir for metadata and clean data
    paths_info = sniffer.PathsInfo(file,
                                   parent_directory,
                                   time=time
                                  )
    paths_info.create_directories()

    log = logger_setup.setup_logger(logger_name='clean_file',
                                    log_file=paths_info.log_file,
                                    level=cfg.logging_level
                                   )

    log.info('file: {}'.format(paths_info.filename))
    log.info('parent_directory: {}'.format(paths_info.parent_directory))

    # clean encoding and special characters
    stdout, stderr = bash_cleaning.fix_encoding(paths_info.file, paths_info.target_file)
    print(stdout)
    # TODO: return log error somewhere?
    stdout, stderr = bash_cleaning.fix_special_characters(paths_info.target_file)
    print(stdout)

    # sniff file, infer types and find characteristics
    log.info('SNIFFING METADATA')
    file_metadata = sniffer.FileMetadata(paths_info,
                                         cfg.check_delimiters,
                                         cfg.date_formats,
                                         cfg.check_n_rows,
                                         cfg.check_acceptable_pct,
                                         check_random_rows=cfg.check_random_rows,
                                         ignore_values=cfg.ignore_values,
                                         time=time
                                        )

    # parse file, check all rows and types for each value
    log.info('PARSING')
    rows_info, counter_values, counter_type_errors = fp.parse_file(paths_info,
                                                                   file_metadata,
                                                                   cfg.erase_unmatched_values)

    file_metadata.add_content_information(rows_info)
    file_metadata.save_metadata()

    # compute basic statictics from values counter
    log.info('COMPUTING STATS')
    stats = fp.basic_statistics(file_metadata,
                                rows_info,
                                counter_values,
                                counter_type_errors,
                                nb_most_frequent=cfg.nb_most_frequent,
                                round_decimal=cfg.round_decimal
                               )

    # save statistics to csv file
    log.info('WRITING STATS')
    stats_filelocation = os.path.join(paths_info.metadata_parent_directory, \
                                 paths_info.path_from_parent_directory, \
                                 paths_info.filename_stem + '_stats.csv')
    stats_delimiter = cfg.target_delimiter if file_metadata.delimiter != cfg.target_delimiter else cfg.stats_file_delimiter
    stats.to_csv(stats_filelocation, sep=stats_delimiter)

    # replace delimiter
    if cfg.delimiter_replacement is True:
        success, information = misc.replace_delimiter(paths_info.target_file,
                                                      file_metadata.delimiter,
                                                      cfg.target_delimiter
                                                     )

    if success:
        log.info(information)
    else:
        log.error(information)

    # close logging files
    for handler in log.handlers:
        log.removeHandler(handler)
        handler.flush()
        handler.close()

def main(file,
         parent_directory):
    """
    Main function
    - initiate a logger, a log file for errors
    - tries to parse the file and clean
    - if successful, ok
    - else log information of FAILURE into stdout

    :Parameters:
        - `file` (str)
            location of file to clean
        - `parent_directory` (str)
            location of its parent_directory

    :Returns: None
    """

    time = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")

    error_file = 'error_{}_{}.log'.format(os.path.basename(file), time)
    if not os.path.exists("logs"):
        os.makedirs("logs")
    error_file_location = os.path.join("logs", error_file)
    errsock = open(error_file_location, 'w')
    sys.stderr = errsock

    try:
        parse_and_clean_csv(file, parent_directory)
        print(file, 'CLEANING SUCCESS')
    except NameError:
        print(file, 'CLEANING FAILED')
        raise NameError(sys.exc_info()[0])
    except AttributeError:
        # occured when using wrong option for log.unset('blah')
        print(file, 'CLEANING FAILED')
        raise AttributeError(sys.exc_info()[0])

    errsock.close()
    if os.stat(error_file).st_size == 0:
        os.remove(error_file)

######### main ####################################################
if __name__ == "__main__":
    # check arguments
    if len(sys.argv) not in [2, 3]:
        print("arg1: file to discover")
        print("arg2: [optional] parent_directory \n \
        (parent_directory for all files in case of recursive discovery \
        of all files in all subdirectories)")
        sys.exit()

    FILE = sys.argv[1]

    if len(sys.argv) == 2:
        PARENT_DIRECTORY = os.path.dirname(FILE)
    else:
        # if optional second argument provided,
        # use it as parent directory from which the path of the file
        # can be reconstructed in order to store
        # cleaned_file and metadata in a mirror directory architecture
        PARENT_DIRECTORY = sys.argv[2]
        if PARENT_DIRECTORY[-1] == '/':
            PARENT_DIRECTORY = PARENT_DIRECTORY[:-1]

    main(FILE, PARENT_DIRECTORY)
