#!/usr/bin/env python
# Author: Heloise Nonne
# Last revised 2017-08-20

import unittest

import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../'))

import pickle
from datetime import datetime
import shutil

from CSVcleaner.parsers.CSVSniffer import *

import tests.config_for_test as cfg

# NOTE: path are not easy to handle here, since we compare dictionaries that contain
# relative paths ('sample_data_types_paths_info.pkl')
# and absolute path (result from instanciation of class PathsInfo)
# hence the trick on the following line
if os.path.basename(os.path.dirname(__file__)) == 'tests':
    os.chdir('tests/')

TEST_DATA_DIR = os.path.join('.', 'data_for_tests')

class TestPathsInfo(unittest.TestCase):
    def setUp(self):
        self.test_file = os.path.join(TEST_DATA_DIR, cfg.testfile_sample_data_types)
        self.test_parent_directory = os.path.dirname(self.test_file)
        self.test_result_pkl = os.path.join(TEST_DATA_DIR, cfg.testfile_sample_data_types_pathsinfo)
        self.test_wrong_result_pkl = os.path.join(TEST_DATA_DIR, cfg.testfile_sample_data_types_filemetadata)

    def test_PathsInfo(self):
        test_paths_info = PathsInfo(self.test_file,
                                    self.test_parent_directory,
                                    time=datetime.now().strftime("%Y-%m-%d_%H:%M:%S"))

        # comparing pkl of PathsInfo does not work since it contains time information
        # and log filenames; we compare dict, having removed unimportant keys
        test_paths_info_dico = test_paths_info.__dict__
        for key in ['time', 'log_file']:
            test_paths_info_dico.pop(key)

        with open(self.test_result_pkl, 'rb') as input:
            test_result = pickle.load(input)

        self.assertTrue(test_paths_info_dico == test_result)

    def test_PathsInfo2(self):
        test_paths_info = PathsInfo(self.test_file,
                                    self.test_parent_directory,
                                    time=datetime.now().strftime("%Y-%m-%d_%H:%M:%S"))

        # comparing pkl of PathsInfo does not work since it contains time information
        # and log filenames; we compare dict, having removed unimportant keys
        test_paths_info_dico = test_paths_info.__dict__
        for key in ['time', 'log_file']:
            test_paths_info_dico.pop(key)

        with open(self.test_wrong_result_pkl, 'rb') as input:
            test_result = pickle.load(input)

        self.assertFalse(test_paths_info_dico == test_result)

    def tearDown(self):
        pass

class TestFileMetadata(unittest.TestCase):
    def setUp(self):
        self.test_file = os.path.join(TEST_DATA_DIR, cfg.testfile_sample_data_types)
        self.test_parent_directory = os.path.dirname(self.test_file)
        self.test_result_pkl = os.path.join(TEST_DATA_DIR, cfg.testfile_sample_data_types_filemetadata)

    def test_FileMetadata(self):
        time = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
        test_paths_info = PathsInfo(self.test_file,
                                    self.test_parent_directory,
                                    time=time)

        # must create directories and copy file into clean directory since
        # FileMetadata reads target_file in clean directory
        # this is because bash_cleaning is supposed to have been used before
        # (here not necessary since sample_data_types.csv has correct encoding and
        # no special characters)
        test_paths_info.create_directories()
        shutil.copy(test_paths_info.file, test_paths_info.target_file)

        test_file_metadata = FileMetadata(test_paths_info,
                                          cfg.check_delimiters,
                                          cfg.date_formats,
                                          cfg.check_n_rows,
                                          cfg.check_acceptable_pct,
                                          check_random_rows=cfg.check_random_rows,
                                          ignore_values=cfg.ignore_values,
                                          time=time)

        # comparing pkl of FileMetadata does not work since it contains paths_info,
        # time information and a logger; we compare dict, having removed unimportant keys
        test_metadata_dico = test_file_metadata.__dict__
        for key in cfg.file_metadata_ignore_keys:
            test_metadata_dico.pop(key)

        with open(self.test_result_pkl, 'rb') as input:
            test_result = pickle.load(input)

        self.assertTrue(test_metadata_dico == test_result)

    def tearDown(self):
        shutil.rmtree(TEST_DATA_DIR + '_clean')
        shutil.rmtree(TEST_DATA_DIR + '_logs')
        shutil.rmtree(TEST_DATA_DIR + '_metadata')

if __name__ == '__main__':
    unittest.main()
