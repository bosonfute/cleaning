#!/bin/bash
# Author: Heloise Nonne
# Last revised 2016-04-09
# Remove ugly characters such as windows encoding, etc. for all files in directory and subdirectories
# WARNING: do not edit outside of vi; ugly characters in the regex (see function remove_ugly) cannot be edited by most text editors !!!)

NB_ARGS=1
E_OPTERROR=85

file=$1

usage() {
    echo "usage: $0 file target"
    echo "Remove special characters such as windows encoding, etc. for all files in directory and subdirectories."
    echo "WARNING!!!!! Do NOT edit outside of vi; special characters in the regex (see function `remove_special_characters`) cannot be edited by most text editors.)"
}

remove_special_characters()
{
    # TODO: try to concatenate the 2 steps
    # weird spaces
    sed -i -e '{s/\xC2\xA0//g}' $1
    # end of lines such as dos ^M # TODO: replace by hexa encoding
    sed -i -e '{s///g;s///g;s///g;s///g;s///g;s///g;s///g;s///g;s///g}' $1
    echo -e "${1}_CHARACTERS_CLEANING_SUCCESSFUL"
}
export -f remove_special_characters

main() {
    remove_special_characters $file
}

if [ $# -ne "$NB_ARGS" ] # script invoked with wrong nb of arguments
then
    usage
    exit $E_OPTERROR
fi

main
