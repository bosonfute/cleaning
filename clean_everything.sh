#!/bin/bash
# Author: Heloise Nonne
# Last revised 2017-03-10
# cleans all files in mydirectory  and its subdirectories(filename lowercase, encoding, bad characters, shake file, find metadata and parse and fix content, save metadata and stats about contents)

NB_ARGS=1
E_OPTERROR=85

# set internal field separator to strict-mode (filenames with space must be considered as a single item in the array)
IFS=$'\n\t'

parent_directory=$1


usage() {
    echo "usage: $0 mydirectory"
    echo "cleans all files in mydirectory and its subdirectories (filename lowercase, encoding, bad characters, shake file, find metadata and parse and fix content, save metadata and stats about contents)."
}

command_exists () {
    command -v "$1" 2> /dev/null ;
}
export -f command_exists

clean_file () {
    python3 CSVcleaner/clean_file.py $1 $2
    echo "$1: DONE"
}
export -f clean_file

main() {
    logtime=$(date +'%Y-%m-%d_%H:%M:%S')
    basename_directory=$(basename $parent_directory)
    logfile="logs/cleaning_"$basename_directory"_"$logtime".log"
    touch $logfile

    #bash utils/unzip_all_files.sh $1 > unzip.log
    # TODO document
    bash scripts/lowercase_nospace_filename.sh $parent_directory 2>&1 | tee $logfile

    # if command parallel exists on the system use it, otherwise loop over files
    # TODO: put arguments for find (not name, etc.) in a single variable
    if command_exists parallel ; then
        find $parent_directory -type f -not -name "*.log" -not -name "*_corrupted*" -not -name "*_type_errors* -not -name '*.zip'" | parallel --no-notice clean_file {} $parent_directory 2>&1 | tee $logfile
    else
        files=($(find $parent_directory -type f -not -name "*.log" -not -name "*_corrupted*" -not -name "*_type_errors* -not -name '*.zip'"))

        for f in ${files[*]}; do
            clean_file $f $parent_directory 2>&1 | tee $logfile
        done
    fi
}


if [ $# -ne "$NB_ARGS" ]
then # script invoked with wrong nb of arguments
    usage
    exit $E_OPTERROR
fi

main
