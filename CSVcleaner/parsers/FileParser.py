#!/usr/bin/env python
# Author: Heloise Nonne
# Last revised 2017-03-09
""" This module parses a file and for each row and each value,
- `parse_file` separates correct row from corrupted ones (where the nb of fields does not match \
the specification in the FileMetadata object)
- `parse_file` performs transformations specified by an object FileMetadata \
(float, datetime convertions, etc.) and keep tracks + logs rows where it could not perform the \
transformation
- `parse_file` keeps tracks of the number of occurence of each value in counters
- `basic_statistics` compute basic_statistics from the counters returns by `parse_file`
"""

import os
import csv
import copy

from CSVcleaner.parsers import data_types as fnt

def fix_row(row,
            row_index,
            file_metadata,
            counter_values,
            counter_type_errors,
            type_errors_log_file,
            erase_unmatched_values=False
           ):
    """
    Fixes one row, according to the instruction store in `file_metadata`
    Function called by the `parse_file` function.

    - `file_metadata.columns_types` contains the infered types for each column;
    - `file_metadata.columns_operations` contains instructions on how to fix values for each column;
    - type error: if fixing the value (i.e. US float format to standard, datetime to iso format) \
    does not work, the function considers it as a type error, increment a counter of type error \
    `counter_type_errors` for each column and logs type error information in \
    file `type_errors_log_file`;
    - if `erase_unmatched_values`=True, the value causing the type error is replaced by '';\
    the replacement is logged in file `type_errors_log_file` for memo.
    - values counter: at the same type, a counter `counter_values` keeps track of the number \
    of occurences for each value encountered.

    :Parameters:
        - `row` (list of strings)
            row as read by csvreader with the correct delimiter
        - `row_index` (int)
            the index of row to fix (used to log errors in `type_errors_log_file`)
        - `file_metadata` (class CSVSniffer.FileMetadata)
            contains file metadata found on the file (meaning attributes of class \
            must have been instanciated by running corresponding methods)
        - `counter_values` (dict)
            keys: file columns\n
            values: class collections.Counter already instanciated where occuring \
            values will be incremented
        - `counter_type_errors` (dict)
            keys: file columns\n
            values: instanciated counter (int) of the nb of error found. Will be \
            incremented if a type error is found in the row.
        - `type_errors_log_file` (str)
            location of the file that stored the type errors encountered
        - `erase_unmatched_values` (bool, default=False)
            False: do not erase value that do not match the type of \
            the column in file_metadata.columns_types \n
            True: file_parse will replace by empty string the values \
            whose type does not match with `file_metadata.columns_types` schema \n
            Ex: if column type is supposed to be an 'int', and the value is 'ABCD'; \n
            if column type is supposed to be `date_french` and the value is '12/29/2017'; this is because the value has `date_us` and transformation to isodate will fail

    :Return:
        - `row` (list)
            cleaned row with fixed values
        - `type_error_row` (bool)
            True if one type error has been encountered. False otherwise.
    """

    type_error_row = False

    # for each column fix data types if necessary and increment counters
    for col_index, col in enumerate(file_metadata.columns):

        type_error_col = False
        row[col_index] = row[col_index].strip()
        initial_value = copy.copy(row[col_index])

        # if empty do nothing
        if row[col_index] == '':
            # increment counter with value
            counter_values[col].update({'':1})
            continue

        # if in ignore_values, replace by empty string and count as empty (i.e. nan)
        if row[col_index] in file_metadata.ignore_values:
            row[col_index] = ''
            # increment counter with value
            counter_values[col].update({'':1})
            continue

        # fix data if there is an operation to make
        if file_metadata.columns_operations[col]:
            row[col_index], type_error_col = fnt.fix_value(row[col_index],
                                                           file_metadata.columns_types[col],
                                                           file_metadata.columns_operations[col]
                                                          )

        if not type_error_col:
            # increment counter with value
            counter_values[col].update({row[col_index]:1})

        else: # handle and log type errors
            type_error_row = True
            counter_type_errors[col] += 1

            error_location = 'row: {}, col: {}={} of type: {}, operation: {} \t' \
                             .format(row_index, col, col_index, file_metadata.columns_types[col],
                                     file_metadata.columns_operations[col])

            if erase_unmatched_values:
                row[col_index] = ''
                # increment counter of emptied values
                counter_values[col].update({'emptied_value':1})

                error_description = 'replacement: {} by {}' \
                                    .format(initial_value, row[col_index])
            else:
                # increment counter with value
                counter_values[col].update({row[col_index]:1})

                error_description = 'replacement not performed on: {}' \
                                    .format(row[col_index])

            # log in errors file
            type_errors_log_file.write(error_location + error_description + '\n')

    return row, type_error_row

def parse_file(paths_info,
               file_metadata,
               erase_unmatched_values=False
               ):
    """ Parse file row by row
    - perform columns operation (how to fix values according to the column infered type) \
    specified in `file_metadata.columns_operations`
    - count occurences of all values encountered and store them in a dictionary of counters \
    (one counter (`collections.Counter`) for each column)
    - count occurences of type errors (value does not match infered type and fixing operation \
    failed) in a counter (one counter (`int`) for each column)

    :Parameters:
        - `paths_info` (class CSVSniffer.PathsInfo)
            class containing all information about file paths, parent directories, etc.
        - `file_metadata` (class CSVSniffer.FileMetadata)
            contains file metadata found on the file (meaning attributes of class \
            must have been instanciated by running corresponding methods)
        - `erase_unmatched_values` (bool, default=False)
            if True, file_parse will replace by empty string the values \
            whose type does not match with `file_metadata.columns_types` schema \n
            Ex: if column type is supposed to be an 'int', and the value is 'ABCD'; \n
            if column type is supposed to be `date_french` and the value is '12/29/2017'; this is because the value has `date_us` and transformation to isodate will fail

    :Return:
        - `rows_info` (dict)
            counter the following counts: rows, corrupted rows, correct_rows, \
            rows_with_type_error
        - `counter_values` (dict)
            keys: the file's columns;\n
            values: class collections.Counter that count the number of occurences \
            of each value for each column. Needed for further use in order to \
            compute statistics (min, max, most frequent values, etc.)
        - `counter_type_errors` (dict)
            keys: file columns; \n
            values: counter (int) of the nb of type error found.
    """

    from collections import Counter

    # open writers to dispatch correct rows and corrupted rows (according to number of fields)
    file_metadata.logger.info('FileParser PARSING {}'.format(paths_info.target_file))

    correct_rows_filename = os.path.join(paths_info.clean_parent_directory,
                                         paths_info.path_from_parent_directory,
                                         paths_info.filename_stem) + \
                                         '_correct.csv'
    correct_rows_file = open(correct_rows_filename, 'w')

    corrupted_rows_filename = os.path.join(paths_info.clean_parent_directory,
                                           paths_info.path_from_parent_directory,
                                           paths_info.filename_stem) + \
                                           '_corrupted.csv'
    corrupted_rows_file = open(corrupted_rows_filename, 'w')

    type_errors_log_filename = os.path.join(paths_info.metadata_parent_directory,
                                            paths_info.path_from_parent_directory,
                                            paths_info.filename_stem) + \
                                            '_type_errors.log'
    type_errors_log_file = open(type_errors_log_filename, 'w')

    # open file
    data_input_file = open(paths_info.target_file, 'r')
    data = csv.reader(data_input_file, delimiter=file_metadata.delimiter)

    # skip header if it exists
    if file_metadata.header is True:
        data.__next__()

    # write header into clean file
    file_metadata.logger.info('Writing header')
    correct_rows_file.write(file_metadata.delimiter.join(file_metadata.columns) + '\n')

    # initialize dictionary of counters of values and type errors in each column
    file_metadata.logger.info('Initializing counters')
    counter_values = {}
    counter_type_errors = {}
    for col in file_metadata.columns:
        counter_values[col] = Counter()
        counter_type_errors[col] = 0

    # count number of rows (rows, corrupted rows, correct_rows, rows_with_type_error )
    rows_info = {}
    rows_info['nb_rows'] = 0
    # count nb of rows with wrong nb of fields
    rows_info['nb_corrupted_rows'] = 0
    # count nb of rows with correct nb of fields
    rows_info['nb_correct_rows'] = 0
    # count nb of rows with type_errors
    rows_info['nb_rows_with_type_error'] = 0

    # parse file row by row
    file_metadata.logger.info('Starting parsing')
    for row_index, row in enumerate(data):
        rows_info['nb_rows'] += 1

        # if nb of fields does not correspond, do nothing and save
        if len(row) != file_metadata.nb_of_fields:
            rows_info['nb_corrupted_rows'] += 1
            corrupted_rows_file.write(file_metadata.delimiter.join(row) + '\n')
            continue

        rows_info['nb_correct_rows'] += 1

        # for each column fix data types when necessary, increment counters
        #file_metadata.logger.debug('--- Fixing row {}'.format(row_index))
        row, type_error_row = fix_row(row,
                                      row_index,
                                      file_metadata,
                                      counter_values,
                                      counter_type_errors,
                                      type_errors_log_file,
                                      erase_unmatched_values)

        if type_error_row:
            rows_info['nb_rows_with_type_error'] += 1

        # save row
        correct_rows_file.write(file_metadata.delimiter.join(row) + '\n')

    file_metadata.logger.info('Closing all files')
    data_input_file.close()
    correct_rows_file.close()
    corrupted_rows_file.close()
    type_errors_log_file.close()

    # log
    file_metadata.logger.info('OK FILE PARSED')
    file_metadata.logger.info('found: {} rows'.format(rows_info['nb_rows']))

    # if type errors were detected
    if rows_info['nb_rows_with_type_error'] != 0:
        file_metadata.logger.info('found: {} rows with type errors' \
                                  .format(rows_info['nb_rows_with_type_error']))
    # if no type_error found, remove empty file
    else:
        os.remove(type_errors_log_filename)

    # if no corrupted rows, remove empty file
    if rows_info['nb_correct_rows'] == rows_info['nb_rows']:
        os.remove(corrupted_rows_filename)
    else:
        file_metadata.logger.warning('found: {} corrupted rows (nb fields mismatch)' \
                                     .format(rows_info['nb_corrupted_rows']))

    # when finished mv correct row file to target
    os.remove(paths_info.target_file)
    os.rename(correct_rows_filename, paths_info.target_file)

    return rows_info, counter_values, counter_type_errors

def basic_statistics(file_metadata,
                     rows_info,
                     counter_values,
                     counter_type_errors,
                     nb_most_frequent,
                     round_decimal):
    """ Compute basic statistics for each column of the file and store result as a csv.
    Must be executed after the `parse_file` function, as it take input from it.

    :Parameters:
        - `file_metadata` (class CSVSniffer.FileMetadata)
            contains file metadata found on the file (meaning attributes of class \
            must have been instanciated by running corresponding methods)
        - `rows_info` (dict)
            counter the following counts: rows, corrupted rows, correct_rows, \
            rows_with_type_error
        - `counter_values` (dict)
            keys: the file's columns;\n
            values: class collections.Counter that count the number of occurences \
            of each value for each column. Needed for further use in order to \
            compute statistics (min, max, most frequent values, etc.)
        - `counter_type_errors` (dict)
            keys: file columns; \n
            values: counter (int) of the nb of type error found.
        - `nb_most_frequent` (int, default=10)
            will compute the `nb_most_frequent` most frequent values for each column
        - `round_decimal` (int, default=2)
            the number of decimals to keep for the statistics and percentages

    :Returns:
        - `stats` (pandas.DataFrame)
            DataFrame containing the computed statistics as well as information
            on the contant and cleaning of each column.
    """

    import pandas as pd

    stats = {}
    for col, counter in counter_values.items():
        stats_col = {}

        stats_col['col_type'] = file_metadata.columns_types[col]
        stats_col['performed_operation'] = file_metadata.columns_operations[col]
        stats_col['type_errors_nb'] = counter_type_errors[col]
        stats_col['type_errors_pct'] = round(counter_type_errors[col] / float(rows_info['nb_rows']),
                                             round_decimal)

        stats_col['empty_values_nb'] = counter['']
        stats_col['empty_values_pct'] = round(counter[''] / float(rows_info['nb_rows']),
                                              round_decimal)

        # number of values that have been emptied because of a type error
        stats_col['emptied_values_nb'] = counter['emptied_value']
        stats_col['emptied_values_pct'] = round(counter['emptied_value'] / float(rows_info['nb_rows']),
                                                round_decimal)

        # we don't take empty values into account in the nb of distinct_values and stats
        del counter['']
        del counter['emptied_value_nb']

        # nb of disctinct values apart from empty strings
        stats_col['nb_distinct_values'] = len(set(counter))

        # min and max
        keys = counter.keys()
        if len(keys) != 0:
            stats_col['min'] = min(keys)
            stats_col['max'] = max(keys)
        else:
            stats_col['min'] = ''
            stats_col['max'] = ''

        # most frequent values
        for i, tuple_val in enumerate(counter.most_common()[:nb_most_frequent]):
            stats_col[str(i+1) + '_most_freq_val'] = tuple_val[0]
            stats_col[str(i+1) + '_most_freq_nb'] = tuple_val[1]
            stats_col[str(i+1) + '_most_freq_pct'] = round(tuple_val[1] / float(rows_info['nb_rows']),
                                                           round_decimal)

        stats[col] = stats_col

    # convert to a dataframe
    stats = pd.DataFrame(stats)
    # reorder the df columns
    stats = stats[file_metadata.columns]

    # reorder the df's index
    stats_final_index = ['nb_distinct_values',
                         'col_type',
                         'performed_operation',
                         'type_errors_nb',
                         'type_errors_pct',
                         'empty_values_nb',
                         'empty_values_pct',
                         'emptied_values_nb',
                         'emptied_values_pct',
                         'min',
                         'max']

    for i in range(nb_most_frequent):
        stats_final_index += [str(i+1) + '_most_freq_val',
                              str(i+1) + '_most_freq_nb',
                              str(i+1) + '_most_freq_pct']

    stats = stats.reindex(stats_final_index)

    return stats
