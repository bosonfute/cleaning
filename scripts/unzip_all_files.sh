#!/bin/bash
# Author: Heloise Nonne
# Last revised 2016-04-11
# Unzip all files in directory and subdirectories.

NB_ARGS=1
E_OPTERROR=85

# set internal field separator to strict-mode (filenames with space must be considered as a single item in the array)
IFS=$'\n\t'

parent_directory=$1


usage() {
    echo "usage: $0 directory"
    echo "Unzip all files in directory and subdirectories."
}


main() {
    # unzip all files
    files=($(find $parent_directory -type f -name "*.zip"))
    # rename files
    for f in ${files[*]}; do
        dirname=$(dirname $f)
        name=$(basename $f)
        cd $dirname
        unzip $name && rm $name
    done
}


if [ $# -ne "$NB_ARGS" ]
then # script invoked with wrong nb of arguments
    usage
    exit $E_OPTERROR
fi

main
