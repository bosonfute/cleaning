#!/usr/bin/env python
# Author: Heloise Nonne
# Last revised 2017-03-09


"""
    A module that checks a csv file of a sample of rows.
    - counts the nb fields;
    - identifies the delimiter;
    - verifies presence of header;
    - identifies column types and tries to see how float or date can be fixed (isoformat).
    Then parses the file to
    - fix formats,
    - replace delimiter if option selected,
    - and makes basic statistics about the columns contents (min, max, mean, occurences, etc.).
"""

import sys
import os
import re
import csv
import random
import json

import logging

import numpy as np
import pandas as pd

import CSVcleaner.parsers.data_types as fnt
from CSVcleaner.utils import misc


######### input / output directories ################
class PathsInfo():
    #TODO: treating as a dictionary: should refactor into a dict?
    """ Class that contains relative and absolute path to fetch data \
        and save metadata, logs and cleaned data

    :Parameters:
        - `file` (str)
            original file to clean
        - `parent_directory` (str)
            location of parent directory where the file lies \
            or if the script is run on a directory, the directory where all subdirectories \
            and files to clean are located
        - `time` (str, default='')
            timestamp of time of execution

    :Attributes:
        - `file` (str)
            raw file location
        - `target_file` (str)
            target clean file location
        - `filename` (str)
            file name
        - `filedirectory` (str)
            directory where the file is located
        - `parent_directory` (str)
            directory where all subdirectories and files to clean are located
        - `path_from_parent_directory` (str)
            relative path from parent_directory
        - `metadata_parent_directory` (str)
            directory where metadata and logs will be saved \
            (mirroring the parent_directory structure)
        - `clean_parent_directory` (str)
            directory where clean data will be saved (mirroring the parent_directory structure)

    """
    def __init__(self,
                 file,
                 parent_directory,
                 time=''
                ):

        # name file and path

        self.file = file
        self.filename = os.path.basename(file)
        if len(self.filename.split('.')) > 1:
            self.filename_stem = ''.join(self.filename.split('.')[:-1])
        else:
            self.filename_stem = self.filename

        self.filedirectory = os.path.dirname(file)
        self.parent_directory = parent_directory

        self.time = time

        # determine relative path from parent_directory
        if self.parent_directory == self.filedirectory:
            self.path_from_parent_directory = ''
        else:
            self.path_from_parent_directory = os.path.relpath(os.path.dirname(self.file),
                                                              self.parent_directory)

        # metadata and clean data parent directories that mirror raw data directories
        if self.parent_directory != '':
            # directory in which metadata will be stored
            self.metadata_parent_directory = \
                os.path.join(os.path.dirname(self.parent_directory),
                             os.path.basename(self.parent_directory)) + '_metadata'

            # directory in which clean data will be stored
            self.clean_parent_directory = \
                os.path.join(os.path.dirname(self.parent_directory),
                             os.path.basename(self.parent_directory)) + '_clean'

            # directory in which logs will be stored
            self.logs_parent_directory = \
                os.path.join(os.path.dirname(self.parent_directory),
                             os.path.basename(self.parent_directory)) + '_logs'

        else:
            self.metadata_parent_directory = "metadata"
            self.clean_parent_directory = "clean"
            self.logs_parent_directory = "logs"

        self.target_file = os.path.join(self.clean_parent_directory,
                                        self.path_from_parent_directory,
                                        self.filename)
        self.log_file = os.path.join(self.logs_parent_directory, self.filename) + \
                                     '_' + self.time + '.log'

    def __cmp__(self, other):
        keys = ['filename', 'parent_directory',
                'file', 'path_from_parent_directory',
                'logs_parent_directory', 'target_file',
                'metadata_parent_directory', 'clean_parent_directory',
                'filename_stem', 'filedirectory']
        compare = True
        for key in keys:
            compare *= (self.__dict__[key] == other.__dict__[key])

        return compare == 1

    def create_directories(self):
        """ Create metadata and clean data directories if they do not exist.
        """
        if not os.path.exists(self.metadata_parent_directory):
            os.makedirs(self.metadata_parent_directory)
        if not os.path.exists(os.path.join(self.metadata_parent_directory,
                                           self.path_from_parent_directory)):
            os.makedirs(os.path.join(self.metadata_parent_directory,
                                     self.path_from_parent_directory))
        if not os.path.exists(self.clean_parent_directory):
            os.makedirs(self.clean_parent_directory)
        if not os.path.exists(os.path.join(self.clean_parent_directory,
                                           self.path_from_parent_directory)):
            os.makedirs(os.path.join(self.clean_parent_directory,
                                     self.path_from_parent_directory))
        if not os.path.exists(self.logs_parent_directory):
            os.makedirs(self.logs_parent_directory)


######### file metadata #############################
class FileMetadata():
    """ Class containing file metadata \
    (date of file parsing, delimiters, nb of rows, fields, etc.)

    :Parameters:
        - `paths_info` (class PathsInfo)
            class containing file location and directories informations
        - `check_delimiters` (list of str)
            list of possible delimiters to try when checking the file
        - `check_date_formats` (list of regex)
            list of possible date and time formats \
            to identify when checking the file
        - `check_n_rows` (int)
            number of rows to consider when checking file
        - `check_acceptable_pct` (float)
            percentage of non matching formats to accept a given format
        - `check_random_rows` (bool)
            if True, consider n_rows randomly taken in the file \
            (to avoid history/seasonality effect by looking just \
            at the beginning of the file)
        - `ignore_values` (list)
            values to ignore, replace by empty string and count as empty
        - `time` (str, default='')
            timestamp of time of execution

    :Attributes:
        - `nb_of_rows` (int)
            number of rows in the file
        - `delimiter` (str)
            field delimiter
        - `candidates_delimiters` (list of str)
            list of candidates if several possible delimiters
        - `nb_of_fields` (int)
            number of fields in file (most common found with `delimiter`)
        - `header` (bool)
            True if first row is a header
        - `columns_types` (dict)
            key=columns; values= identified type of column
        - `columns_operations` (dict)
            key=columns; values=operation to perform
            to fix the serie when parsing the entire file if identified;
            None otherwise;

    """

    def __init__(self,
                 paths_info,
                 check_delimiters,
                 check_date_formats,
                 check_n_rows,
                 check_acceptable_pct,
                 check_random_rows,
                 ignore_values,
                 time=''
                ):

        self.logger = logging.getLogger('clean_file.auxiliary')
        self.logger.debug('FCT CSVSniffer.FileMetadata.__init__')
        self.time = time

        self.paths_info = paths_info

        self.check_n_rows = check_n_rows
        self.check_random_rows = check_random_rows
        self.check_acceptable_pct = check_acceptable_pct
        self.check_delimiters = check_delimiters
        self.check_date_formats = check_date_formats
        self.ignore_values = ignore_values

        self.nb_of_rows = self.count_nb_rows()
        self.identify_delimiter_and_fields()
        self.check_header()
        self.columns, self.columns_types, self.columns_operations = self.check_column_types()

    def __cmp__(self, other):
        return self.__dict__ == other.__dict__

    def count_nb_fields(self):
        """ For each row to check, counts the number of fields for each possible delimiter.

        :return: a dictionary with keys=delimiters
            and value = list of length check_n_rows with nb of fields for each row
        """
        self.logger.debug('FCT CSVSniffer.FileMetadata.count_nb_fields')
        # initialize dict of found nb of fields for all possible delimiters
        nb_fields = {}

        for delimiter in self.check_delimiters:
            nb_fields[delimiter] = []

        # open file
        data_input = open(self.paths_info.target_file, 'r')
        data = csv.reader(data_input)

        # read nrows lines to guess the delimiter
        for row in range(self.check_n_rows):
            # some files have problematic commas
            # (instead of point as decimal separators for instance),
            # that causes data.__next__() to split a single line
            # into several elements in a list;
            #''.join(data.__next__()) handles that problem
            try:
                line = ''.join(data.__next__())
            except StopIteration:
                break
            except ValueError:
                # some exception arose with some files (can't remember which);
                # may need to change ValueError to another type
                self.logger.error('problem reading line:', row)
                print('\t problem reading: ', self.paths_info.target_file, 'line:', row)
                sys.exit()
            # for each possible delimiter, compute the number of fields
            for delimiter in self.check_delimiters:
                nb_fields[delimiter].append(len(line.split(delimiter)))

        # close file
        data_input.close()

        return nb_fields

    def identify_delimiter_and_fields(self):
        """ Identify the best candidate delimiter
        among several possible delimiters `check_delimiters`
        and count the nb of fields on the first `check_n_rows`
        """
        from collections import Counter
        self.logger.debug('FCT CSVSniffer.FileMetadata.identify_delimiter_and_fields')

        non_uniform_nb_fields = []
        unique_field = []

        # TOCHECK: is this correct to call the method with self?
        nb_fields = self.count_nb_fields()

        # check each possible delimiter and retain candidates
        for delimiter in self.check_delimiters:
            # if some but few lines have different nb of fields,
            # take median: that should be the correct nb of fields
            nb_of_fields = int(np.floor(np.median(nb_fields[delimiter])))

            # pass if nb_fields is one
            if nb_of_fields == 1:
                continue

            # if always the same number of fields, that is the correct delimiter
            if len(set(nb_fields[delimiter])) == 1:
                unique_field.append(delimiter)
                continue

            # pass if nb of fields is too different for the nrows lines
            # acceptable_pct is the acceptable percentage of wrong lines to select the delimiter
            if len(set(nb_fields[delimiter])) > self.check_n_rows * self.check_acceptable_pct:
                continue

            # accept delimiter as candidate (non_uniform_nb_fields)
            # when nb of lines with different nb of fields is within the acceptable_pct;
            # that case should take care of cases where commentaries have end of line inside
            if len(set(nb_fields[delimiter])) <= self.check_n_rows * self.check_acceptable_pct:
                non_uniform_nb_fields.append(delimiter)
                continue

        # if no candidate: exit
        if (len(unique_field) == 0) and (len(non_uniform_nb_fields) == 0):
            self.logger.warning('no candidate delimiter found')
            sys.exit()

        # if several candidates, warning
        if len(unique_field) > 1:
            self.logger.warning("warning: several candidates")
        elif (len(unique_field) == 0) and (len(non_uniform_nb_fields) > 1):
            self.logger.warning("warning: several non uniform candidates")

        # identify best candidate delimiter
        # if several, take the first one, we don't know how to discriminate
        if len(unique_field) > 0:
            self.delimiter = unique_field[0]
            self.candidates_delimiters = unique_field
        if (len(unique_field) == 0) and (len(non_uniform_nb_fields) > 0):
            self.delimiter = non_uniform_nb_fields[0]
            self.candidates_delimiters = non_uniform_nb_fields

        self.logger.info('OK found delimiter: %s', self.delimiter)

        # for the candidate delimiter, count the various nb of fields found
        # and pick the most common number among lines
        counts = Counter(nb_fields[self.delimiter]).most_common()

        # take the most common nb_of_fields
        self.nb_of_fields = counts[:1][0][0]
        self.logger.info('OK found: %s fields', self.nb_of_fields)
        # if 1, take the second most common, this happens when there are EOL in comments
        if self.nb_of_fields == 1:
            self.nb_of_fields = counts[1:2][0][0]

        # TODO: debug this part (handling EOL characters in comments)
        # if len(counts) > 2:
        #    fixed_end_of_lines = check_end_of_line_in_file(directory, file, delimiter)
        #    self.logger.warning(probably end of line characters in comments: check manually)

    def check_header(self):
        """ Check if the first line contains elements with a least 2 alpha characters
        """
        self.logger.debug('FCT CSVSniffer.FileMetadata.check_header')


        # read first row
        data_input = open(self.paths_info.target_file, 'r')
        data = csv.reader(data_input, delimiter=self.delimiter)
        line = data.__next__()
        data_input.close()

        # if all columns match at least 2 consecutive alpha char, then file has header
        check_header = list(map(lambda elt: bool(re.match(r'.*[a-zA-Z]{2,}.*', elt))
                                or elt == '', line))

        #all_fields_probable_headers = bool(functools.reduce(lambda x, y: x*y, check_header))

        nb_fields_probable_headers = sum(check_header)

        if nb_fields_probable_headers / self.nb_of_fields == 1:
            self.header = True
            self.logger.info('OK header detected')
        elif nb_fields_probable_headers / self.nb_of_fields >= 0.5:
            self.header = True
            self.logger.info('OK: probable header detected')
            self.logger.warning('>50% of fields are probable headers; decision = there is a header')
        else:
            self.header = False
            self.logger.warning('no header detected')

        if self.header and (len(line) != self.nb_of_fields):
            self.logger.warning('file nb_of_fields and nb fields in header mismatch')

    def check_column_types(self):
        """ Identifies the column types (int, float, date, string),
        the operations to perform to fix the values
        by parsing `check_n_rows` rows in file `paths_info.target_file`

        :return:
            :list columns: list of column names
            :dict columns_types: key = column, val = type
            :dict columns_operations: key = column, val = operation
        """
        self.logger.debug('FCT CSVSniffer.FileMetadata.check_column_types')
        self.logger.info("INFERING COLUMN TYPES")

        # choose random rows to skip
        if (self.check_random_rows) and (self.check_n_rows < self.nb_of_rows):
            # skip nb_rows - check_n_rows while reading
            skiprows = sorted(random.sample(range(self.nb_of_rows),
                                            self.nb_of_rows - self.check_n_rows))
        else:
            skiprows = []

        # read file
        # TODO merge the 2 conditions with option header=...
        # TODO check that when no header, columns are really named Unnamed (i'm not sure)
        if self.header:
            if 0 in skiprows:
                skiprows.remove(0)

            sample_df = pd.read_csv(self.paths_info.target_file,
                                    sep=self.delimiter,
                                    nrows=self.check_n_rows,
                                    skiprows=skiprows,
                                    dtype=str,
                                    keep_default_na=False,
                                    error_bad_lines=False)
            # rename unnamed columns
            columns = []
            for i in range(self.nb_of_fields):
                if 'Unnamed' in sample_df.columns[i]:
                    columns.append('col_' + str(i))
                else:
                    columns.append(sample_df.columns[i])
            sample_df.columns = columns

        else:
            columns = ['col_' + i for i in range(self.nb_of_fields)]
            sample_df = pd.read_csv(self.paths_info.target_file,
                                    sep=self.delimiter,
                                    nrows=self.check_n_rows,
                                    skiprows=skiprows,
                                    header=None,
                                    names=columns,
                                    error_bad_lines=False)

        columns_types = {}
        columns_operations = {}

        for col in sample_df.columns:
            columns_types[col] = None
            columns_operations[col] = None

            columns_types[col], columns_operations[col] = \
                fnt.identify_serie_type(sample_df[col],
                                        self.check_date_formats,
                                        acceptable_pct=self.check_acceptable_pct,
                                        min_size=None,
                                        ignore_values=self.ignore_values)

        self.logger.info("OK column types infered")

        return columns, columns_types, columns_operations

    def count_nb_rows(self):
        """ Counts the number of rows with bash and python.
        Alerts (log error) if there is a difference.
        """
        self.logger.debug('FCT CSVSniffer.FileMetadata.count_nb_rows')

        nb_rows_bash = misc.count_nb_rows_bash(self.paths_info.target_file)
        nb_rows_py = misc.count_nb_rows_py(self.paths_info.target_file)

        if nb_rows_bash != nb_rows_py:
            self.logger.error('inconsistent nb of rows between py and bash method')

        return nb_rows_bash

    def metadata_to_json(self):
        """ Transform metadata into json format """
        self.logger.debug('FCT CSVSniffer.FileMetadata.metadata_to_json')

        dict_to_save = self.__dict__.copy()
        # extract from object paths_info
        paths_info = dict_to_save.pop('paths_info').__dict__
        dict_to_save.update(paths_info)
        # remove logger object from dict
        dict_to_save.pop('logger')

        json_metadata = json.dumps(dict_to_save)

        return json_metadata

    def save_metadata(self):
        """ Save metadata as json file """
        self.logger.debug('FCT CSVSniffer.FileMetadata.save_metadata')

        json_metadata = self.metadata_to_json()
        metadata_file = os.path.join(self.paths_info.metadata_parent_directory,
                                     self.paths_info.filename) + \
                        '_metadata.json'

        self.logger.info('WRITING METADATA TO JSON %s', metadata_file)
        with open(metadata_file, 'w') as outfile:
            json.dump(json_metadata, outfile, ensure_ascii=False)

    def add_content_information(self, rows_info):
        """ Creates new attributes with the number of rows that are
        - correct
        - corrupted
        - with type errors

        :Parameters:
            - `rows_info` (dict)
                dictionary built by function `parse_file` in `FileParser`
                containing information about rows
        """
        self.logger.debug('FCT CSVSniffer.FileMetadata.add_content_information')

        self.nb_corrupted_rows = rows_info['nb_corrupted_rows']
        self.nb_correct_rows = rows_info['nb_correct_rows']
        self.nb_rows_with_type_error = rows_info['nb_rows_with_type_error']
