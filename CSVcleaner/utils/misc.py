#!/usr/bin/env python
# Author: Heloise Nonne
# Last revised 2017-03-09
"""
Various functions used by CSVSniffer classes
- count the number of rows with bash
- count the number of rows with python
- check presence of a string anywhere in a file
- replace delimiter by another (target) delimiter
"""

import shlex
import subprocess
from subprocess import PIPE
import csv

def count_nb_rows_bash(file):
    """ Count nb of rows in `file` with bash wc command """
    process = subprocess.Popen(['wc', '-l', file], stdout=PIPE)
    stdout, stderr = process.communicate()
    nb_rows = stdout.decode('utf8').strip('\n').split(' ')[0]
    nb_rows = int(nb_rows)

    return nb_rows

def count_nb_rows_py(file):
    """ Count nb of rows in `file` with python """
    with open(file, 'r') as data_input:
        data = csv.reader(data_input)

        # count number of rows
        nb_rows = 0

        # parse file
        for row in data:
            nb_rows += 1

    return nb_rows

def check_presence_string(file, string):
    """ Use command line grep to check if `string` exists in `file`.
    """
    process1 = subprocess.Popen(['grep', string, file], stdout=PIPE)
    process2 = subprocess.Popen(['wc', '-l'], stdin=process1.stdout, stdout=PIPE)
    process1.stdout.close()
    nb_occurences = process2.communicate()[0].decode().split('\n')[0]

    if nb_occurences == "0":
        return False
    else:
        return True

def replace_delimiter(file,
                      delimiter,
                      target_delimiter
                     ):
    """
    Replace field `delimiter` by `target_delimiter` in `file`.

    :Returns:
        - `success` (bool)
            True if replace was successful, False otherwise
        - `info` (str)
            Information about delimiter replacement or nonreplacement
    """

    if delimiter == target_delimiter:
        info = 'delimiter already is target_delimiter'
        success = True

    # if target_delimiter present in the file, do not replace
    elif check_presence_string(file, target_delimiter):
        info = 'delimiter not replaced: presence within fields'
        success = False

    else:

        process = subprocess.Popen(['sed', '-i', 's/' + delimiter + '/' + target_delimiter + '/g', file],
                                   stdout=PIPE)
        process.wait()

        # check success of replacement
        success = check_presence_string(file, target_delimiter)

        if success:
            info = 'delimiter replacement successful'
        else:
            info = 'delimiter replacement unsuccessful'

    return success, info
