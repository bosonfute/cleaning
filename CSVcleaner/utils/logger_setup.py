"""
Setup of a python logger for writing and return it.
- set log line format
- set FileHandler to "write"
- set level (default=INFO)
"""

import logging
import logging.config

def setup_logger(logger_name, log_file, level=logging.INFO, log_to_stderr=False):
    """
    Setup a logger to store logs. One logger for each cleaned file is used.

    :Parameters:
        - `logger_name` (str)
            name of the object for future reference
        - `log_file` (str)
            name of the target log file the FileHandler will send info to
        - `level` (logging.LEVEL, default=logging.INFO)
            level of information that logging must store
        - `log_to_stderr` (bool, default=False)
            if True, then log into log file AND to stderr as well

    :Return:
        - `l` (logger object)
            logger object to be used in the project

    """

    logger = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(asctime)s -  %(levelname)s -  %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')
    fileHandler = logging.FileHandler(log_file, mode='w')
    fileHandler.setFormatter(formatter)
    logger.setLevel(level)
    logger.addHandler(fileHandler)

    if log_to_stderr: # to log also to stderr
        consoleHandler = logging.StreamHandler()
        consoleHandler.setFormatter(formatter)
        logger.addHandler(consoleHandler)

    return logger
