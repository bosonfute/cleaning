**********************
The CSVcleaner package
**********************

.. contents::

The "clean_file" module
================================
.. automodule:: CSVcleaner.clean_file
   :members:

The "parsers" module
========================

The "CSVSniffer" module
-----------------------
.. automodule:: CSVcleaner.parsers.CSVSniffer
   :members:


The "FileParser" module
-----------------------
.. automodule:: CSVcleaner.parsers.FileParser
   :members:


The "data_types" module
-----------------------
.. automodule:: CSVcleaner.parsers.data_types
  :members:


The "utils" module
===================

The "misc" module
-----------------
.. automodule:: CSVcleaner.utils.misc
   :members:

The "bash_cleaning" module
--------------------------
.. automodule:: CSVcleaner.utils.bash_cleaning
   :members:

The "logger_setup" module
-------------------------
.. automodule:: CSVcleaner.utils.logger_setup
   :members:
