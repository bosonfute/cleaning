#!/usr/bin/env python
# Author: Heloise Nonne
# Last revised 2017-08-20

import unittest

import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../'))

from datetime import datetime
import shutil
import hashlib

from CSVcleaner.parsers.CSVSniffer import *
from CSVcleaner.parsers.FileParser import *

import tests.config_for_test as cfg

# NOTE: path are not easy to handle here, since we compare dictionaries that contain
# relative paths ('sample_data_types_paths_info.pkl')
# and absolute path (result from instanciation of class PathsInfo)
# hence the trick on the following line
if os.path.basename(os.path.dirname(__file__)) == 'tests':
    os.chdir('tests/')

TEST_DATA_DIR = os.path.join('.', 'data_for_tests')

class TestParseFile(unittest.TestCase):
    def setUp(self):
        self.test_file = os.path.join(TEST_DATA_DIR, cfg.testfile_sample_data_types)
        self.test_parent_directory = os.path.dirname(self.test_file)

    def test_parsefile(self):
        time = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
        test_paths_info = PathsInfo(self.test_file,
                                    self.test_parent_directory,
                                    time=time
                                   )

        # must create directories and copy file into clean directory since
        # FileMetadata reads target_file in clean directory
        # this is because bash_cleaning is supposed to have been used before
        # (here not necessary since sample_data_types.csv has correct encoding and
        # no special characters)
        test_paths_info.create_directories()
        shutil.copy(test_paths_info.file, test_paths_info.target_file)

        test_file_metadata = FileMetadata(test_paths_info,
                                          cfg.check_delimiters,
                                          cfg.date_formats,
                                          cfg.check_n_rows,
                                          cfg.check_acceptable_pct,
                                          check_random_rows=cfg.check_random_rows,
                                          ignore_values=cfg.ignore_values,
                                          time=time)

        rows_info, counter_values, counter_type_errors = parse_file(test_paths_info, test_file_metadata)

        sha_result = 'e0104ca0d107994c9209fe99fb1de00a1cac58a18c6b52c91244b63d7d083fff'

        f = open(test_paths_info.target_file, 'rb')
        sha = hashlib.sha256()
        data = f.read()
        sha.update(data)
        f.close()

        self.assertTrue(sha.hexdigest() == sha_result)

    def tearDown(self):
        shutil.rmtree(TEST_DATA_DIR + '_clean')
        shutil.rmtree(TEST_DATA_DIR + '_logs')
        shutil.rmtree(TEST_DATA_DIR + '_metadata')

class TestBasicStatistics(unittest.TestCase):
    def setUp(self):
        self.test_file = os.path.join(TEST_DATA_DIR, cfg.testfile_sample_data_types)
        self.test_parent_directory = os.path.dirname(self.test_file)
        self.test_stats_file = os.path.join(TEST_DATA_DIR + '_metadata', 'temp_stats.csv')

    def test_basic_statistics(self):
        time = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
        test_paths_info = PathsInfo(self.test_file,
                                    self.test_parent_directory,
                                    time=time
                                   )

        # must create directories and copy file into clean directory since
        # FileMetadata reads target_file in clean directory
        # this is because bash_cleaning is supposed to have been used before
        # (here not necessary since sample_data_types.csv has correct encoding and
        # no special characters)
        test_paths_info.create_directories()
        shutil.copy(test_paths_info.file, test_paths_info.target_file)

        test_file_metadata = FileMetadata(test_paths_info,
                                          cfg.check_delimiters,
                                          cfg.date_formats,
                                          cfg.check_n_rows,
                                          cfg.check_acceptable_pct,
                                          check_random_rows=cfg.check_random_rows,
                                          ignore_values=cfg.ignore_values,
                                          time=time)

        rows_info, counter_values, counter_type_errors = parse_file(test_paths_info, test_file_metadata, cfg.erase_unmatched_values)

        stats = basic_statistics(test_file_metadata,
                                 rows_info,
                                 counter_values,
                                 counter_type_errors,
                                 nb_most_frequent=cfg.nb_most_frequent,
                                 round_decimal=cfg.round_decimal
                                )

        stats_delimiter = cfg.target_delimiter if test_file_metadata.delimiter != cfg.target_delimiter else cfg.stats_file_delimiter
        stats.to_csv(self.test_stats_file, sep=stats_delimiter)

        sha_result = 'c5437bc9f4296b5d81b2a0f1847886a5538fb61194801be0841064e79ce63d2e'

        f = open(self.test_stats_file, 'r')
        sha = hashlib.sha256()
        data = f.read()
        # we can only keep the 12 first lines
        # the lines after that in stats are the n most frequent values
        # but the order can change for one execution of basic_statistics to another
        # if same values have the same number of occurences
        # we thus skip the n most frequent values information in the comparison
        data = data.split('\n')[:12]
        data = ''.join(data)
        sha.update(data.encode()) # need to be encoded into binary for sha
        f.close()

        self.assertTrue(sha.hexdigest() == sha_result)

    def tearDown(self):
        os.remove(self.test_stats_file)
        shutil.rmtree(TEST_DATA_DIR + '_clean')
        shutil.rmtree(TEST_DATA_DIR + '_logs')
        shutil.rmtree(TEST_DATA_DIR + '_metadata')


if __name__ == '__main__':
    unittest.main()
