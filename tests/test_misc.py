#!/usr/bin/env python
# Author: Heloise Nonne
# Last revised 2017-08-20

import unittest

import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../'))

import shutil

from CSVcleaner.utils import misc

import tests.config_for_test as cfg

TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), 'data_for_tests')

class TestCount_nb_rows_bash(unittest.TestCase):

    def setUp(self):
        self.file = os.path.join(TEST_DATA_DIR, cfg.testfile_sample_data_types)

    def test_count_nb_rows(self):
        test = 16
        result = misc.count_nb_rows_bash(self.file)
        self.assertTrue(result == test)

    def tearDown(self):
        pass

class TestCount_nb_rows_py(unittest.TestCase):

    def setUp(self):
        self.file = os.path.join(TEST_DATA_DIR, cfg.testfile_sample_data_types)

    def test_count_nb_rows(self):
        test = 16
        result = misc.count_nb_rows_py(self.file)
        self.assertTrue(result == test)

    def tearDown(self):
        pass

class TestCheckPresenceString(unittest.TestCase):

    def setUp(self):
        self.file = os.path.join(TEST_DATA_DIR, cfg.testfile_sample_data_types)

    def testCheckPresenceString(self):
        test = False
        result = misc.check_presence_string(self.file, '|')
        self.assertTrue(result == test)

    def testCheckPresenceString2(self):
        test = True
        result = misc.check_presence_string(self.file, '3')
        self.assertTrue(result == test)

    def tearDown(self):
        pass

class TestReplaceDelimiter(unittest.TestCase):

    def setUp(self):
        self.file = os.path.join(TEST_DATA_DIR, cfg.testfile_sample_data_types)
        self.tempfile = os.path.join(TEST_DATA_DIR, 'temp_sample.csv')
        shutil.copyfile(self.file, self.tempfile)

    def testReplaceDelimiter(self):
        test = True, 'delimiter already is target_delimiter'
        result = misc.replace_delimiter(self.tempfile, ';', ';')
        self.assertTrue(result == test)

    def testReplaceDelimiter2(self):
        test = False, 'delimiter not replaced: presence within fields'
        result = misc.replace_delimiter(self.tempfile, ';', ',')
        self.assertTrue(result == test)

    def testReplaceDelimiter3(self):
        test = True, 'delimiter replacement successful'
        result = misc.replace_delimiter(self.tempfile, ';', '|')
        self.assertTrue(result == test)

    def tearDown(self):
        os.remove(self.tempfile)

if __name__ == '__main__':
    unittest.main()
