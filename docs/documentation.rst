Documentation
=============

.. contents:: Table of Contents

How to use and what it does
---------------------------

First use the script: `clean_everything.sh`:
::

    bash clean_everything.sh my_parent_directory

where `my_parent_directory` is the root directory containing all the files you want to clean.

**What it does for each file (see `CSVcleaner/clean_file.py`):**

#. **Create mirror tree directories** `clean`, `logs` and `metadata` where it stores cleaned files and metadata files
#. Try to **fix encoding** (calling script: `scripts/fix_encoding_file.sh`)
#. Try to **fix special characters** (calling script: `scripts/fix_special_characters_file.sh`)
#. **Sniff the file and identifies the type of each column** and store the result in `metadata` directory in json format
#. **Clean the content of the file**, i.e. for each row

    * count the number of columns (fields)
    * **consider rows that do not match as corrupted and store separately**;
    * otherwise continue and:
    * **fix float format** and replace by standard international format (i.e. 3000574.32)
    * **fix datetime**, date and time format and replace by ISO format (i.e. respectively YYYY-mm-DD HH:MM:SS, YYYY-mm-DD, and HH:MM:SS)
    * **replace the separator** with a pipe `|` (if possible)

#. Compute **basic statistics** for each column (min, max, most current values, etc) and store in `metadata` directory as a csv file

Only the two above-mentioned scripts are bash. All the rest is python and documented below in the API.

**What it does not do:**

* magically fix files that are corrupted
* magically fix end of line characters inside comments that screw up the number of fields in each line
* replace missing values with a magic AI/Big Data Buzz algorithm
* save the world
* coffee (but you may try as a sudoer)

Configuration
-------------

All configuration of execution must be done in `CSVcleaner/config/config.py`

There are 4 parts:
- CSVcleaner.clean_file configuration: setting logging level
- CSVcleaner.CSVSniffer.FileMetadata configuration: setting options such as date_formats to try, values to ignore as nan, target_delimiter, number of rows to sniff
- FileParser.parse_file configuration: whether or not to erase values that do not match the type infered by FileMetadata
- FileParser.basic_statistics: setting options such as the nb of most frequent value to keep, round decimal, etc.

The second part is the more important to set: especially
- `ignore_values`
- `check_acceptable_pct`
- `delimiter_replacement`

Logging
-------

You can set logging level (ERROR, WARNING, INFO, DEBUG) in the config file `config.config.py` (see logging doc: https://docs.python.org/3/library/logging.html#levels). Default is INFO.

Several information are logged:
- information on execution steps for all files treated by the CSVcleaner: in `cleaningtool/logs/cleaning_my_parent_directory_timestamp.log`
Example:
::

    data/data_test/my_data_file.csv:-encoding-conversion-from-us-ascii-SUCCESS
    data/data_test_clean/my_data_file.csv_CHARACTERS_CLEANING_SUCCESSFUL
    data/data_test/my_data_file CLEANING SUCCESS
    data/data_test/my_data_file: DONE
    data/data_test/my_data_file2.csv:-encoding-conversion-from-iso-8859-1-SUCCESS
    data/data_test_clean/my_data_file2.csv_CHARACTERS_CLEANING_SUCCESSFUL
    data/data_test/my_data_file2 CLEANING FAILED
    data/data_test/my_data_file2: DONE

- execution errors for each file separately in `cleaningtool/logs/error_my_data_file.csv_timestamp.log`

Example of an error because the file in data_clean is empty:
::

    Traceback (most recent call last):
      File "CSVcleaner/clean_file.py", line 176, in <module>
        main(FILE, PARENT_DIRECTORY)
      File "CSVcleaner/clean_file.py", line 142, in main
        target_delimiter)
      File "CSVcleaner/clean_file.py", line 87, in parse_and_clean_csv
        rows_info, counter_values, counter_type_errors = fp.parse_file(paths_info, file_metadata)
      File "./CSVcleaner/parsers/FileParser.py", line 200, in parse_file
        data.__next__()
    StopIteration

- for each file parsed and cleaner, logger information on all the step (level for logger is defined in `CSVcleaner/utils/logger_setup.py`, the default being level=logging.INFO); this log is stored in `my_parent_directory_logs/path_to_file/my_data_file.csv_timestamp.log`

Example if everything goes well:
::

    2017-08-18 16:03:46 -  INFO -  file: my_data_file.csv
    2017-08-18 16:03:46 -  INFO -  parent_directory: data/data_test
    2017-08-18 16:03:49 -  INFO -  SNIFFING METADATA
    2017-08-18 16:03:50 -  INFO -  OK found delimiter: |
    2017-08-18 16:03:50 -  INFO -  OK found: 98 fields
    2017-08-18 16:03:50 -  INFO -  OK header detected
    2017-08-18 16:03:50 -  INFO -  INFERING COLUMN TYPES
    2017-08-18 16:04:10 -  INFO -  PARSING
    2017-08-18 16:04:10 -  INFO -  FileParser PARSING data/data_test_clean/my_data_file.csv
    2017-08-18 16:04:10 -  INFO -  Writing header
    2017-08-18 16:04:10 -  INFO -  Initializing counters
    2017-08-18 16:04:10 -  INFO -  Starting parsing
    2017-08-18 16:04:28 -  INFO -  Closing all files
    2017-08-18 16:04:28 -  INFO -  OK FILE PARSED
    2017-08-18 16:04:28 -  INFO -  found: 26057 rows
    2017-08-18 16:04:28 -  INFO -  found: 409 rows with type errors
    2017-08-18 16:04:28 -  INFO -  WRITING METADATA TO JSON data/data_test_metadata/my_data_file.csv_metadata.json
    2017-08-18 16:04:28 -  INFO -  COMPUTING STATS
    2017-08-18 16:04:28 -  INFO -  WRITING STATS
    2017-08-18 16:04:28 -  INFO -  delimiter already is target_delimiter

.. warning::

    - this a useful log to check the cleaning step and identify special case manually
    - you should grep on ERROR and WARNING inside the `my_parent_directory_logs` to see which file may require manual check.

    ::

        grep "ERROR\|WARNING" my_parent_directory_logs/*

    **What you may find:**

    Case 1:
    ::

        my_data_file.csv_2017-08-18_16:03:38.log:2017-08-18 16:03:40 -  ERROR -  inconsistent nb of rows between py and bash method
        my_data_file.csv_2017-08-18_16:03:38.log:2017-08-18 16:03:56 -  WARNING -  found: 1 corrupted rows (nb fields mismatch)

    This indicates corrupted rows, i.e. where the number of fields does not match what it should be. It might be due to an endofline character that has not been cleaned by the script `fix_special_characters`.
    You may ignore if the percentage of corrupted rows (put aside in `my_parent_directory_clean/my_data_file_corrupted.csv`).

    Case 2:
    ::

        my_data_file.csv_2017-08-18_16:03:38.log:2017-08-18 16:03:40 -  WARNING -  CHECK: at least 50% of fields are probable headers -> decision = there is a header

    This is when looking at the first row, CSVSniffer finds out that all field to not match a large string, but most of them are.

    For instance the first row is: `A123;ID_ELEMENT;DATE_EVENEMENT;COMMENT`.

    In this case, the first field is ambigous, it could be a value. Thus at least 50% of the fields are probable headers, but not all. In that situation, CSVSniffer decides the first row is a header.

    If less than 50% of the fields are probable headers, CSVSniffer logs the following line:
    ::

        my_data_file.csv_2017-08-18_16:03:38.log:2017-08-18 16:03:40 -  WARNING -  'no header detected'
