#!/usr/bin/env python
# Author: Heloise Nonne
# Last revised 2017-08-20

import unittest

import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../'))

import pandas as pd

from CSVcleaner.parsers import data_types

import tests.config_for_test as cfg

TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), 'data_for_tests')

class TestTryIntTest(unittest.TestCase):

    def setUp(self):
        self.file = os.path.join(TEST_DATA_DIR, cfg.testfile_sample_data_types)
        self.sample_df = pd.read_csv(self.file,
                                     sep=";",
                                     dtype='str',
                                     keep_default_na=False,
                                     error_bad_lines=False)

    def test_try_int_accept_0(self):
        test = (False, None, None, 0.2)
        result = data_types.try_int(self.sample_df['int'], acceptable_pct=0)
        self.assertTrue(result == test)

    def test_try_int_accept_pct(self):
        test = (True, 'int', 'int', 0.2)
        result = data_types.try_int(self.sample_df['int'], acceptable_pct=0.25)
        self.assertTrue(result == test)

    def test_try_int_minsize(self):
        test = (False, None, None, None)
        result = data_types.try_int(self.sample_df['int'], acceptable_pct=0.25, min_size=50)
        self.assertTrue(result == test)

    def test_try_int_string(self):
        test = (False, None, None, 0.8)
        result = data_types.try_int(self.sample_df['string'])
        self.assertTrue(result == test)

    def tearDown(self):
        pass

class TestTryFloat(unittest.TestCase):

    def setUp(self):
        self.file = os.path.join(TEST_DATA_DIR, cfg.testfile_sample_data_types)
        self.sample_df = pd.read_csv(self.file,
                                     sep=";",
                                     dtype='str',
                                     keep_default_na=False,
                                     error_bad_lines=False)

    def test_try_float_1(self):
        test = (False, None, None, 0.3333333333333333)
        result = data_types.try_float(self.sample_df['float'], acceptable_pct=0)
        self.assertTrue(result == test)

    def test_try_float_2(self):
        test = (True, 'float', 'float', 0.3333333333333333)
        result = data_types.try_float(self.sample_df['float'], acceptable_pct=0.35)
        self.assertTrue(result == test)

    def test_try_float_3(self):
        test = (False, None, None, None)
        result = data_types.try_float(self.sample_df['float'], acceptable_pct=0.35, min_size=50)
        self.assertTrue(result == test)

    def test_try_float_4(self):
        test = (False, None, None, 0.4)
        result = data_types.try_float(self.sample_df['float_french'])
        self.assertTrue(result == test)

    def test_try_float_5(self):
        test = (False, None, None, 0.6)
        result = data_types.try_float(self.sample_df['float_us'])
        self.assertTrue(result == test)

    def test_try_float_6(self):
        test = (False, None, None, 0.8)
        result = data_types.try_float(self.sample_df['string'])
        self.assertTrue(result == test)

    def tearDown(self):
        pass

class TestTryFixFloat(unittest.TestCase):

    def setUp(self):
        self.file = os.path.join(TEST_DATA_DIR, cfg.testfile_sample_data_types)
        self.sample_df = pd.read_csv(self.file,
                                     sep=";",
                                     dtype='str',
                                     keep_default_na=False,
                                     error_bad_lines=False)

    def test_try_fix_float_1(self):
        test = (True, 'float', 'replace_commas_french', 0.0)
        result = data_types.try_fix_float(self.sample_df['float_french'], acceptable_pct=0)
        self.assertTrue(result == test)

    def test_try_fix_float_2(self):
        test = (True, 'float', 'remove_commas_us', 0.0)
        result = data_types.try_fix_float(self.sample_df['float_us'])
        self.assertTrue(result == test)

    def test_try_fix_float_3(self):
        test = (False, None, None, 0.8)
        result = data_types.try_fix_float(self.sample_df['string'])
        self.assertTrue(result == test)

    def tearDown(self):
        pass

class TestCheckDate(unittest.TestCase):

    def setUp(self):
        self.file = os.path.join(TEST_DATA_DIR, cfg.testfile_sample_data_types)
        self.sample_df = pd.read_csv(self.file,
                                     sep=";",
                                     dtype='str',
                                     keep_default_na=False,
                                     error_bad_lines=False)

    def test_check_date_time(self):
        test = (False, None, None, None)
        result = data_types.check_date(self.sample_df['time'],
                                       cfg.date_formats,
                                       acceptable_pct=0)
        self.assertTrue(result == test)

    def test_check_date_time(self):
        test = (True, 'time_iso', '%H:%M:%S', 0.40000000000000002)
        result = data_types.check_date(self.sample_df['time'],
                                       cfg.date_formats,
                                       acceptable_pct=0.5)
        self.assertTrue(result == test)

    def test_check_date_iso(self):
        test = (True, 'date_iso', '%Y-%m-%d', 0.0)
        result = data_types.check_date(self.sample_df['date_iso'],
                                       cfg.date_formats,
                                       acceptable_pct=0)
        self.assertTrue(result == test)

    def test_check_date_french(self):
        test = (True, 'date_french', '%d/%m/%Y', 0.0)
        result = data_types.check_date(self.sample_df['date_french'],
                                       cfg.date_formats,
                                       acceptable_pct=0)
        self.assertTrue(result == test)

    def test_check_date_us(self):
        test = (True, 'date_us', '%m/%d/%Y', 0.0)
        result = data_types.check_date(self.sample_df['date_us'],
                                       cfg.date_formats,
                                       acceptable_pct=0)
        self.assertTrue(result == test)

    def test_check_date_datetime_iso(self):
        test = (True, 'datetime_iso', '%Y-%m-%d %H:%M:%S', 0.0)
        result = data_types.check_date(self.sample_df['datetime_iso'],
                                       cfg.date_formats,
                                       acceptable_pct=0)
        self.assertTrue(result == test)

    def test_check_date_datetime_hourmin(self):
        test = (True, 'datetime_hourmin', '%Y-%m-%d %H:%M', 0.0)
        result = data_types.check_date(self.sample_df['datetime_hourmin'],
                                       cfg.date_formats,
                                       acceptable_pct=0)
        self.assertTrue(result == test)

    def test_check_date_datetime_french_hourmin(self):
        test = (True, 'datetime_french_hourmin', '%d/%m/%Y %H:%M', 0.0)
        result = data_types.check_date(self.sample_df['datetime_french_hourmin'],
                                       cfg.date_formats,
                                       acceptable_pct=0)
        self.assertTrue(result == test)

    def test_check_date_datetime_french(self):
        test = (True, 'datetime_french', '%d/%m/%Y %H:%M:%S', 0.0)
        result = data_types.check_date(self.sample_df['datetime_french'],
                                       cfg.date_formats,
                                       acceptable_pct=0)
        self.assertTrue(result == test)

    def test_check_date_string(self):
        test = (False, None, None, None)
        result = data_types.check_date(self.sample_df['string'],
                                       cfg.date_formats,
                                       acceptable_pct=0)
        self.assertTrue(result == test)

    def tearDown(self):
        pass

class TestFixValue(unittest.TestCase):

    def setUp(self):
        pass

    def test_fix_int(self):
        test = ('3', False)
        result = data_types.fix_value('3', 'int', 'int')
        self.assertTrue(result == test)

    def test_fix_float(self):
        test = ('2.3', False)
        result = data_types.fix_value('2.3', 'float', 'float')
        self.assertTrue(result == test)

    def test_fix_float_2(self):
        test = ('2,3', True)
        result = data_types.fix_value('2,3', 'float', 'float')
        self.assertTrue(result == test)

    def test_fix_french_float(self):
        test = ('2.3', False)
        result = data_types.fix_value('2,3', 'float_french', 'replace_commas_french')
        self.assertTrue(result == test)

    def test_fix_us_float(self):
        test = ('3000000.5', False)
        result = data_types.fix_value('3,000,000.5', 'float_us', 'remove_commas_us')
        self.assertTrue(result == test)

    def test_fix_time(self):
        test = ('09:23:54', False)
        result = data_types.fix_value('09:23:54', 'time_iso', '%H:%M:%S')
        self.assertTrue(result == test)

    def test_fix_time_2(self):
        test = ('09:2', True)
        result = data_types.fix_value('09:2', 'time_iso', '%H:%M:%S')
        self.assertTrue(result == test)

    def test_fix_time_3(self):
        test = ('hello', True)
        result = data_types.fix_value('hello', 'time_iso', '%H:%M:%S')
        self.assertTrue(result == test)

    def test_fix_date_iso(self):
        test = ('2016-06-23', False)
        result = data_types.fix_value('2016-06-23', 'date_iso', '%Y-%m-%d')
        self.assertTrue(result == test)

    def test_fix_date_french(self):
        test = ('2016-12-29', False)
        result = data_types.fix_value('29/12/2016', 'date_french', '%d/%m/%Y')
        self.assertTrue(result == test)

    def test_fix_date_french_2(self):
        test = ('12/29/2016', True)
        result = data_types.fix_value('12/29/2016', 'date_french', '%d/%m/%Y')
        self.assertTrue(result == test)

    def test_fix_date_us(self):
        test = ('2016-12-29', False)
        result = data_types.fix_value('12/29/2016', 'date_us', '%m/%d/%Y')
        self.assertTrue(result == test)

    def test_fix_date_us_2(self):
        test = ('29/12/2016', True)
        result = data_types.fix_value('29/12/2016', 'date_us', '%m/%d/%Y')
        self.assertTrue(result == test)

    def test_fix_datetime_iso(self):
        test = ('2016-06-23 12:23:39', False)
        result = data_types.fix_value('2016-06-23 12:23:39', 'datetime_iso', '%Y-%m-%d %H:%M:%S')
        self.assertTrue(result == test)

    def test_fix_datetime_hourmin(self):
        test = ('2016-12-30 23:18:00', False)
        result = data_types.fix_value('2016-12-30 23:18', 'datetime_hourmin', '%Y-%m-%d %H:%M')
        self.assertTrue(result == test)

    def test_fix_datetime_french_hourmin(self):
        test = ('2016-12-30 22:39:00', False)
        result = data_types.fix_value('30/12/2016 22:39', 'datetime_french_hourmin', '%d/%m/%Y %H:%M')
        self.assertTrue(result == test)

    def test_fix_datetime_french(self):
        test = ('2016-10-27 14:15:35', False)
        result = data_types.fix_value('27/10/2016 14:15:35', 'datetime_french', '%d/%m/%Y %H:%M:%S')
        self.assertTrue(result == test)

    def tearDown(self):
        pass

if __name__ == '__main__':
    unittest.main()
