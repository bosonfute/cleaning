************
bash scripts
************

3 bash scripts are used by CSVcleaner and stored in scripts directory:

- `lowercase_no_space_filename.sh`
- `fix_encoding.sh`
- `fix_special_characters.sh`

In addition `unzip_all_files.sh` can be useful before running the cleaner. It unzips all files in parent_directory and its subdirectories and removes the `.zip` archives
