"""
Config file for CSVcleaner
"""
##########################
# clean_file configuration
##########################
# logger level
import logging
logging_level = logging.INFO

############################
# FileMetadata configuration
############################
# nb of rows to consider when checking file
check_n_rows = 100000
# random rows: True if the check_n_rows have to be picked randomly from the csv file
check_random_rows = True
# percentage of non matching formats to accept a given format
check_acceptable_pct = 0.25
# delimiters to try while sniffing file
check_delimiters = ["|", ";", "\t", ","]
# values to be replaced by empty values and count as empty (i.e. nan)
ignore_values = ['Non Renseigné']
# delimiter replacement: True if the parsing must try to replace the delimiter; False otherwise
delimiter_replacement = True
# target delimiter to use if delimiter_replacement=True
target_delimiter = '|'

# date, hour, datetime formats to try while checking file
date_formats = {
                'time_iso': ("%H:%M:%S", r'^[0-9]{2}:[0-9]{2}:[0-9]{2}$'),
                'date_iso': ("%Y-%m-%d", r'^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$'),
                'date_french': ("%d/%m/%Y", r'^[0-3]{0,1}[0-9]{1}/[0-1]{0,1}[0-9]{1}/[0-9]{4}$'),
                'date_us': ("%m/%d/%Y", r'^[0-1]{0,1}[0-9]{1}/[0-3]{0,1}[0-9]{1}/[0-9]{4}$'),
                'datetime_iso': ("%Y-%m-%d %H:%M:%S", \
                    r'^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2} [0-9]{2}:[0-9]{2}:[0-9]{2}$'),
                'datetime_hourmin': ("%Y-%m-%d %H:%M", \
                    r'^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2} [0-9]{2}:[0-9]{2}$'),
                'datetime_french_hourmin': ("%d/%m/%Y %H:%M", \
                    r'^[0-3]{0,1}[0-9]{1}/[0-1]{0,1}[0-9]{1}/[0-9]{4} [0-9]{2}:[0-9]{2}$'),
                'datetime_french': ("%d/%m/%Y %H:%M:%S", \
                    r'^[0-3]{0,1}[0-9]{1}/[0-1]{0,1}[0-9]{1}/[0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2}$')
               }

#####################################
# FileParser.parse_file configuration
#####################################
# True: erase values that do not match the type infered by FileMetadata and replace by empty string; False, warn in log file but do nothing
erase_unmatched_values = False

###########################################
# FileParser.basic_statistics configuration
###########################################
# up to how many most frequent values must be stores in the file metadata statistics
nb_most_frequent = 10
# round decimal: how many decimal to keep in statistics (pct, etc.)
round_decimal = 2
# statistics file delimiter
stats_file_delimiter = ';'
