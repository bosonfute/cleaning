"""
Function that call bash scripts
- change encoding to utf8
- remove special characters in file
"""
import subprocess
from subprocess import PIPE

# TODO: handle relative path when executing outside of project root dir
def fix_encoding(file, target):
    """ Change `file` encoding to utf8 is possible and store result in `target`.
    If encoding conversion fails, copy anyway to target.
    """
    process = subprocess.Popen(['scripts/fix_encoding.sh', file, target], stdout=PIPE)
    stdout, stderr = process.communicate()
    stdout = stdout.decode('utf8').strip('\n').split(' ')[0]
    return stdout, stderr

def fix_special_characters(file):
    """ Remove special characters """
    process = subprocess.Popen(['scripts/fix_special_characters.sh', file], stdout=PIPE)
    stdout, stderr = process.communicate()
    stdout = stdout.decode('utf8').strip('\n').split(' ')[0]
    return stdout, stderr
