#!/bin/bash
# Author: Heloise Nonne
# Last revised 2016-04-09
# Convert encoding to utf-8 for all files in directory and subdirectories

NB_ARGS=2
E_OPTERROR=85

file=$1
target=$2

usage() {
    echo "usage: $0 file target"
    echo "convert encoding to utf-8"
}

convert_encoding() {
    # temporary file
    infile=$1
    outfile=$2

    # find out the encoding
    encoding=$(file -i $infile | cut -d '=' -f2)

    # convert to utf-8
    if [[ $encoding == "unknown-8bit" ]]; then
        iconv -f ISO-8859-1 -t utf-8 $infile -o $outfile;
    else
        iconv -f $encoding -t utf-8 $infile -o $outfile;
    fi


    # error handling
    if [[ "$?" -ne "0" ]]; then
        rm $outfile;
        cp $infile $outfile
        echo "$infile:-encoding-conversion-from-$encoding-FAILED";
    else
        echo "$infile:-encoding-conversion-from-$encoding-SUCCESS"
    fi
}
export -f convert_encoding

main() {
    convert_encoding $file $target
}

if [ $# -ne "$NB_ARGS" ]
then # script invoked with wrong nb of arguments
    usage
    exit $E_OPTERROR
fi

main
