#!/usr/bin/env python
# Author: Heloise Nonne
# Last revised 2017-08-20

import unittest

import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../'))

import tests.config_for_test as cfg

import shutil
import hashlib

from CSVcleaner.utils import bash_cleaning

TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), 'data_for_tests')

class TestFixEncoding(unittest.TestCase):

    def setUp(self):
        self.file = os.path.join(TEST_DATA_DIR, cfg.testfile_encoding)
        self.file2 = os.path.join(TEST_DATA_DIR, cfg.testfile_encoding2)
        self.tempfile = os.path.join(TEST_DATA_DIR, 'temp.csv')

    def testFixEncodingExecution(self):
        fct_output = (self.file + ':-encoding-conversion-from-utf-16le-SUCCESS',
         None)
        result = bash_cleaning.fix_encoding(self.file, self.tempfile)
        self.assertTrue(result == fct_output)

    def testFixEncodingExecution2(self):
        fct_output = (self.file2 + ':-encoding-conversion-from-iso-8859-1-SUCCESS',
         None)
        result = bash_cleaning.fix_encoding(self.file2, self.tempfile)
        self.assertTrue(result == fct_output)

    def testFixEncodingResultingFile(self):
        sha_result = '170780d9d11d87dbcc48cb150e75d93303798212635cd7442003bb6c5243872f'

        bash_cleaning.fix_encoding(self.file, self.tempfile)
        f = open(self.tempfile, 'rb')
        sha = hashlib.sha256()
        data = f.read()
        sha.update(data)
        f.close()

        self.assertTrue(sha.hexdigest() == sha_result)

    def testFixEncodingResultingFile2(self):
        sha_result = 'f513ad2f35408d6bd98a02fa14d9025dc497403834eae9f5290ca00a0969a18d'

        bash_cleaning.fix_encoding(self.file2, self.tempfile)
        f = open(self.tempfile, 'rb')
        sha = hashlib.sha256()
        data = f.read()
        sha.update(data)
        f.close()

        self.assertTrue(sha.hexdigest() == sha_result)

    def tearDown(self):
        os.remove(self.tempfile)

class TestFixSpecialCharacters(unittest.TestCase):

    def setUp(self):
        self.tempfile = os.path.join(TEST_DATA_DIR, 'temp.csv')
        self.file = os.path.join(TEST_DATA_DIR, cfg.testfile_special_characters)
        shutil.copyfile(self.file, self.tempfile)

    def testFixSpecialCharactersExecution(self):
        fct_output = (self.tempfile + '_CHARACTERS_CLEANING_SUCCESSFUL', None)
        result = bash_cleaning.fix_special_characters(self.tempfile)
        self.assertTrue(result[1] == fct_output[1])

    def testFixSpecialCharactersFile(self):
        sha_result = '33cf1ddaa1d6dc55f43841e5c4a83217229daac4c5982fdcf57319abfad5c908'

        bash_cleaning.fix_special_characters(self.tempfile)

        f = open(self.tempfile, 'rb')
        sha = hashlib.sha256()
        data = f.read()
        sha.update(data)
        f.close()

        self.assertTrue(sha.hexdigest() == sha_result)

    def tearDown(self):
        os.remove(self.tempfile)

if __name__ == '__main__':
    unittest.main()
