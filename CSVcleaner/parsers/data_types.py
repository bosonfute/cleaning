#!/usr/bin/env python
# Author: Heloise Nonne
# Last revised 2016-12-11
"""
Functions that allow to
- assert if a string is an int, a float or a date
- identify which format (date format, float format = std, French or US)
- identify the type of a pandas serie \
    and which operation to perform to transform to standard format if needed.
"""

import numpy as np

###### integers #############################

def try_int(serie, acceptable_pct=0, min_size=None):
    """ Try to cast a serie as integer.

    :Parameters:
        - `serie`: (pandas.Series)
            serie to try to cast as int
        - `acceptable_pct` (float)
            (default=0) the acceptable percentage of rows not cast as int
        - `min_size` (int):
            (default=0.5*serie.size) the minimum size required \
            to conclude on the series type

    :Return:
        - `type_found` (bool)
            True if casting as integer succeeded
        - `serie_type` (str)
            'int' if succeeded; None otherwise
        - `serie_operation` (str)
            operation to perform when parsing the entire file: \
            'int' if succeeded; None otherwise;
        - `pct_wrong` (float)
            percentage of values that do not match
    """

    def is_int(elt):
        """ tries to cast as int """
        try:
            elt = int(elt)
            return True
        except ValueError:
            return False

    if not min_size:
        min_size = np.floor(0.50 * serie.size)

    type_found = False
    serie_type, serie_operation = None, None
    pct_wrong = None

    if serie.size > min_size: # dont try if size is too small
        wrong = serie[~serie.apply(is_int)]
        pct_wrong = wrong.size / float(serie.size)

        if pct_wrong <= acceptable_pct:
            type_found = True
            serie_type = "int"
            serie_operation = 'int'

    return (type_found, serie_type, serie_operation, pct_wrong)

###### floats #############################
#TODO: merge try_int and try_float and use a closure?
def try_float(serie, acceptable_pct=0, min_size=None):
    """ Try to cast a serie as float.

    :Parameters:
        - `serie`: (pandas.Series)
            serie to try to cast as float
        - `acceptable_pct` (float)
            (default=0) the acceptable percentage of rows not cast as int
        - `min_size` (int):
            (default=0.5*serie.size) the minimum size required \
            to conclude on the series type

    :Return:
        - `type_found` (bool)
            True if casting as integer succeeded
        - `serie_type` (str)
            'float' if succeeded; None otherwise
        - `serie_operation` (str)
            'float' if succeeded; None otherwise; \
            operation to perform on the serie when parsing the entire file
        - `pct_wrong` (float)
            percentage of values that do not match
    """
    def is_float(elt):
        """ tries to cast as float """
        try:
            elt = float(elt)
            return True
        except ValueError:
            return False

    if not min_size:
        min_size = np.floor(0.50 * serie.size)

    type_found = False
    serie_type, serie_operation = None, None
    pct_wrong = None

    if serie.size > min_size:
        wrong = serie[~serie.apply(is_float)]
        pct_wrong = wrong.size / float(serie.size)

        if pct_wrong <= acceptable_pct:
            type_found = True
            serie_type = 'float'
            serie_operation = 'float'

    return (type_found, serie_type, serie_operation, pct_wrong)

def try_fix_float(serie, acceptable_pct=0, min_size=None):
    """ Tries to fix a serie casting as float, \
    taking into account possibility of French and US float formats.

    :Parameters:
        - `serie` (pandas.Series)
            serie to try to cast as int
        - `acceptable_pct` (float)
            (default=0) the acceptable percentage of rows not cast as int
        - `min_size` (int)
            (default=0.5*serie.size) the minimum size required \
            to conclude on the series type

    :Return:
        - `type_found` (bool)
            True if succeeded to cast as float
        - `serie_type` (str)
            None if not a float, otherwise 'float64', 'us_float64'
        - `serie_operation` (str)
            the operation to perform to fix the float format \
            (replace ',' as '.' if French; remove ',' if US)
        - `pct_wrong` (float)
            percentage of values that do not match
    """
    type_found = False
    serie_type, serie_operation = None, None
    pct_wrong = None

    # try to replace commas by dots: handles case of floats such as 1,3 -> replace `,` by `.`
    type_found, serie_type, serie_operation, pct_wrong = try_float(serie.str.replace(',', '.'),
                                                                   acceptable_pct,
                                                                   min_size)

    if type_found:
        serie_type = 'float'
        serie_operation = 'replace_commas_french'
        return (type_found, serie_type, serie_operation, pct_wrong)

    # try us format: case floats are like: 4,000.11 -> remove `,`
    type_found, serie_type, serie_operation, pct_wrong = try_float(serie.str.replace(',', ''),
                                                                   acceptable_pct,
                                                                   min_size)

    if type_found:
        serie_type = 'float'
        serie_operation = 'remove_commas_us'
        return (type_found, serie_type, serie_operation, pct_wrong)

    return (type_found, serie_type, serie_operation, pct_wrong)

###### Date #############################
def check_date(serie, date_formats, acceptable_pct=0, min_size=None):
    """ Checks if the content of a serie are date, time or datetime \
        against a list of possible formats given by `date_formats`.

    :Parameters:
        - `serie` (pandas.Series)
            serie to try to cast as int
        - `date_formats` (dict)
            dict of possible date and time formats \
            to identify when checking the file \n
            key=date format name; \n
            values = tuples (datetime format, regex) \n
            Ex: key = 'date_french' and value=('%d/%m/%Y', '^[0-3]{0,1}[0-9]{1}/[0-1]{0,1}[0-9]{1}/[0-9]{4}$')
        - `acceptable_pct` (float)
            (default=0) the acceptable percentage of rows not cast as int
        - `min_size` (int)
            (default=0.5*serie.size) the minimum size required \
            to conclude on the series type

    :return:
        - `type_found` (bool)
            True if a format matched
        - `serie_type` (str)
            None if nothing found, 'date', 'time' or 'datetime' otherwise
        - `serie_operation` (str)
            None if nothing found, date format otherwise
        - `pct_wrong` (float)
            percentage of value that do not match
    """

    def match_regex(mystring, regex):
        """ Check if a string matched regular expression `regex` and returns a boolean.
        """
        import re

        match = bool(re.match(re.compile(regex), mystring))
        return match

    def try_transform_date_to_iso(elt, date_format):
        """ Tries to transform a string to datetime format according to format
        given by `date_format`.

        :Parameters:
            - `elt` (str)
                string to transform into iso
            - `date_format` (str)
                format of date that string is supposed to have
                ex: "%H:%M:%S", "%Y-%m-%d %H:%M:%S", "%m/%d/%Y"

        :Return:
            - `elt` converted to iso format; np.nan if not successful
        """
        from datetime import datetime
        elt = str(elt)
        try:
            date_iso = datetime.strptime(elt, date_format)
        except ValueError:
            date_iso = np.nan

        return date_iso

    if not min_size:
        min_size = np.floor(0.50 * serie.size)

    type_found = False
    serie_type, serie_operation = None, None

    # try converting to date
    for date_type, (date_format, regex) in date_formats.items():
        if serie.size > min_size:
            matches = serie.apply(lambda elt: match_regex(elt, regex))
            pct_wrong = 1. - (matches.sum() / float(serie.size))

            if pct_wrong <= acceptable_pct:
                type_found = True
                serie_type = date_type
                serie_operation = date_format
                break

    # handling ambiguity if date_format
    if serie_operation == '%d/%m/%Y':
        # try to change to iso with format '%d/%m/%Y'
        serie = serie.apply(lambda elt: try_transform_date_to_iso(elt, '%d/%m/%Y'))
        pct_wrong_format_1 = serie.isnull().sum() / float(serie.size)
        # try to change to iso with format '%m/%d/%Y'
        serie = serie.apply(lambda elt: try_transform_date_to_iso(elt, '%m/%d/%Y'))
        pct_wrong_format_2 = serie.isnull().sum() / float(serie.size)

        if pct_wrong_format_2 < pct_wrong_format_1:
            pct_wrong = pct_wrong_format_2
            serie_type = 'date_us'
            serie_operation = '%m/%d/%Y'
        else:
            pct_wrong = pct_wrong_format_1

    if not type_found:
        pct_wrong = None

    return (type_found, serie_type, serie_operation, pct_wrong)

###### Find serie type #############################
def identify_serie_type(serie, date_formats, acceptable_pct=0, min_size=None, ignore_values=[]):
    """ Identify type of a serie.
    (Used by CSVSniffer.FileMetadata)

    :Parameters:
        - `serie` (pandas.Series)
            serie which type to indentify
        - `date_formats` (dict)
            dict of possible date and time formats \
            to identify when checking the file
            key=date format name; values = tuples (datetime format, regex)
        - `acceptable_pct` (float)
            (default=0) the acceptable percentage of rows not cast as int
        - `min_size` (int)
            (default=0.5*serie.size) the minimum size required \
            to conclude on the series type
        - `ignore_values` (list)
            list of value to ignore (default=[])

    :Return:
        - `serie_type` (str)
            type of serie if identified; str otherwise
        - `serie_operation` (str)
            if identified, operation to perform in order to fix type format \
            when parsing the entire file; None otherwise;
    """
    type_found = False

    serie = serie[(serie.notnull()) & (~serie.isin(ignore_values))].astype('str')

    # try int, float and fix floats
    # fix float = replace commas by dots to handle case of floats (1,34)
    #             or handle us format (1,000.44))
    type_found, serie_type, serie_operation, pct_wrong = try_int(serie, acceptable_pct, min_size)
    if type_found:
        return serie_type, serie_operation

    # try casting as float
    type_found, serie_type, serie_operation, pct_wrong = try_float(serie, acceptable_pct, min_size)
    if type_found:
        return serie_type, serie_operation

    # try to fix floats, i.e.:
    type_found, serie_type, serie_operation, pct_wrong = try_fix_float(serie)
    if type_found:
        return serie_type, serie_operation

    # try to convert to date
    type_found, serie_type, serie_operation, pct_wrong = check_date(serie,
                                                                    date_formats,
                                                                    acceptable_pct,
                                                                    min_size)
    if type_found:
        return serie_type, serie_operation

    serie_type = 'str'
    serie_operation = None

    return serie_type, serie_operation

###### Fixing values #############################
def fix_value(value, type_data, operation):
    """ Fix `value` doing `operation` according to its type.
    (Used by FileParser)

    :Parameters:
        - `value` (str)
            value to fix
        - `type_data` (str)
            type of data value is supposed to be or become after fixing
        - `operation` (str)
            type of operation that has been identify as possibly necessary to
            obtain the `type`. Possible values are ('replace_commas_french', 'remove_commas_us' or regex to transform dateformat to iso format -- see config.date_formats)

    :Returns:
        - `new_value` (str)
            the new value after fixing
        - `error` (bool)
            True if fixing was unsuccessful; False if it worked

    """
    from datetime import datetime

    def iso_time(value, operation):
        """ tries to transform time to format: HH:MM:SS """
        error = False
        try:
            value = datetime.strptime(value, operation) \
                            .isoformat(' ')[-8:] # take only time
        except ValueError:
            error = True
        return value, error

    def iso_date(value, operation):
        """ tries to transform date to format: YYYY-mm-DD """
        error = False
        try:
            value = datetime.strptime(value, operation) \
                            .isoformat(' ')[:10] # take only date
        except ValueError:
            error = True
        return value, error

    def iso_datetime(value, operation):
        """ tries to transform datetime to format: YYYY-mm-DD HH:MM:SS """
        error = False
        try:
            value = datetime.strptime(value, operation) \
                            .isoformat(' ')
        except ValueError:
            error = True
        return value, error

    def fix_float(value, operation):
        """
        if `operation` = replace_commas_french, replace french comma in float by a dot
        if `operation` = remove_commas_us, remove commas (thousand separators)
        """
        error = False
        if operation == 'replace_commas_french':
            try:
                value = value.replace(',', '.')
                float(value)
            except ValueError:
                error = True
            return value, error

        elif operation == 'remove_commas_us':
            try:
                value = value.replace(',', '')
                float(value)
            except ValueError:
                error = True
            return value, error

        else:
            try:
                float(value)
            except ValueError:
                error = True
            return value, error

    def fix_int(value, operation):
        error = False
        try:
            int(value)
        except ValueError:
            error = True
        return value, error

    fix_value_options = {'time_iso': iso_time,
                         'date_us': iso_date,
                         'date_french': iso_date,
                         'date_iso': iso_date,
                         'datetime_iso': iso_datetime,
                         'datetime_hourmin': iso_datetime,
                         'datetime_french_hourmin': iso_datetime,
                         'datetime_french': iso_datetime,
                         'float': fix_float,
                         'float_french': fix_float,
                         'float_us': fix_float,
                         'int': fix_int
                        }

    if type_data not in fix_value_options.keys():
        raise ValueError('{} not in supported type fixing operations'.format(type_data))

    new_value, error = fix_value_options[type_data](value, operation)

    return new_value, error

##############################################
def main():

    import unittest
    unittest.main()

if __name__ == '__main__':
    main()
